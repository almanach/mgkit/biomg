package MyRegExp;

use warnings;
use strict;
use Exporter;

use vars qw($VERSION $RE @EXPORT @ISA);
@ISA = qw(Exporter);
@EXPORT = qw($RE);
$VERSION = '1.0';

$RE = {};

sub add_pattern {
  my ($key,$pat) = @_;
  $RE->{$key} = $pat;
}

######################################################################
## NUMBERS

add_pattern
  comment => qr{ \s+(?:\{\{(.+?)\}\})?\s* }oix;

add_pattern
  int_number => qr{ (?: \d+ (?: $RE->{comment} (?:\|[,.]) \d+)* ) }oix;

add_pattern 
  unite => qr{(?:deux|trois|quatre|cinq|six|sept|huit|neuf)}oix;

add_pattern 
  first => qr{(?:un(?:e)?)}oix;

add_pattern 
  xunite=> qr{(?: $RE->{first} | $RE->{unite} )}ox;

add_pattern 
  dizaine => qr{(?: (?:dix(?:-(sept|huit|neuf))?)
		| onze
		| douze
		| treize
		| quatorze
		| quinze
		| seize
	       )
	      }oix;

add_pattern 
  xdizaine => qr{(?: (?:(?:quatre-)?vingt)
		 | trente
		 | quarante
		 | cinquante
		 | soixante
		 | septante
		 | octante
		 | huitante
		 | nonante
		)
	       }oix;

add_pattern 
  ydizaine => qr{(?:soixante|quatre-vingt)}oix;

add_pattern 
  dizaines => qr{(?: $RE->{dizaine}
		 |  (?:$RE->{xdizaine} (?: $RE->{comment} et $RE->{comment} $RE->{first} | -$RE->{unite})? )
		 |  $RE->{ydizaine}-$RE->{dizaine}
		)
	       }oix;

add_pattern
  centaines => qr{ (?: (?: $RE->{int_number} | $RE->{unite}$RE->{comment})? cent  (?: $RE->{comment} (?:$RE->{xunite}|$RE->{dizaine}))? )
                   | (?: $RE->{unite} $RE->{comment} cents )
		}oix;

add_pattern
  milliers => qr{ (?: (?: $RE->{int_number} | $RE->{centaines} |  $RE->{dizaines} | $RE->{unite} ) $RE->{comment} )?
                 mil(?:le(?:s)?)?
                 (?: $RE->{comment} (?: $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) )?
	      }oix;

add_pattern
  millions => qr{ (?: (?: $RE->{int_number} | $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) $RE->{comment} )?
                 million
                 (?: $RE->{comment} (?: $RE->{milliers} | $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) )?
	       }oix;

add_pattern
  milliards => qr{ (?: (?: $RE->{int_number} | $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) $RE->{comment} )?
                 milliard(?:s)?
                 (?: $RE->{comment} (?: $RE->{millions} | $RE->{milliers} | $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) )?
	      }oix;

add_pattern 
  number => qr{(?: 
	       $RE->{milliards} 
	       | $RE->{millions}
	       | $RE->{milliers}
	       | $RE->{centaines}
	       | $RE->{dizaines}
	       | $RE->{unite}
	       | $RE->{int_number}
	      )
	     }oix;


######################################################################
## Ordinals

add_pattern
  term_ord => qr{(?:i�me(?:s)?)}oix;

add_pattern 
  unite_ord => qr{(?:deux|trois|quatr|cinqu|six|sept|huit|neuv)}oix;

add_pattern
  first_ord => qr{un}oix;


add_pattern 
  xunite_ord => qr{(?: $RE->{first_ord} | $RE->{unite_ord} )}ox;


add_pattern 
  dizaine_ord => qr{(?: (?:dix(?:-(sept|huit|neuv))?)
		    | onz
		    | douz
		    | treiz
		    | quatorz
		    | quinz
		    | seiz
		   )
		  }oix;

add_pattern 
  xdizaine_ord => qr{(?: (?:(?:quatre-)?vingt)
		     | trent
		     | quarant
		     | cinquant
		     | soixant
		     | septant
		     | octant
		     | huitant
		     | nonant
		    )
		   }oix;

add_pattern 
  dizaines_ord => qr{(?: $RE->{dizaine_ord}
		     |  (?:$RE->{xdizaine} (?: $RE->{comment} et $RE->{comment} $RE->{first_ord} | -$RE->{unite_ord})? )
		     |  $RE->{ydizaine}-$RE->{dizaine_ord}
		    )
	       }oix;

add_pattern
  centaines_ord => qr{ (?:$RE->{unite} $RE->{comment} )? cent  (?: $RE->{comment} (?:$RE->{xunite_ord} | $RE->{dizaine_ord}))? }oix;

add_pattern
  milliers_ord => qr{ (?: (?: $RE->{centaines} |  $RE->{dizaines} | $RE->{unite} ) $RE->{comment} )? 
		      (?: mille $RE->{comment} (?: $RE->{centaines_ord} |  $RE->{dizaines_ord} | $RE->{xunite_ord} )
		      | mill
		    )
		   }oix;

add_pattern
  millions_ord => qr{ (?: (?: $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) $RE->{comment} )?
		      (?: million
		          $RE->{comment} (?: $RE->{milliers_ord} | $RE->{centaines_ord} |  $RE->{dizaines_ord} | $RE->{xunite_ord} )
		      | millionn
		      )
		   }oix;

add_pattern
  milliards_ord => qr{ (?: (?: $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) $RE->{comment} )?
		       milliard
		       (?: $RE->{comment} (?: $RE->{millions_ord} | $RE->{milliers_ord} | $RE->{centaines_ord} |  $RE->{dizaines_ord} | $RE->{xunite_ord} ) )?
	      }oix;

add_pattern 
  ordinal => qr{(?: premier(?:s)?
		| premi�re(?:s)?
		| second(?:e)?(?:s)?
		| (?:  $RE->{milliards_ord} 
		| $RE->{millions_ord}
		       | $RE->{milliers_ord}
		| $RE->{centaines_ord}
		| $RE->{dizaines_ord}
		| $RE->{unite_ord}
	       )
		$RE->{term_ord}
	       )
	      }oix;

######################################################################
## Dates

add_pattern
  mois => qr{ (?: janvier | janv\.?
	     | f�vrier | fevr?\.?
	     | mars?
	     | avril | avr\.?
	     | mai
	     | jui?n
	     | juillet | jui?l\.?
	     | ao�t
	     | septembre | sept\.?
	     | octobre | oct\.?
	     | novembre | nov\.?
	     | d�cembre | d�c\.?
	   )
	  }oix;

add_pattern
  jour => qr{ (?: lundi | mardi | mercredi | jeudi | vendredi | samedi | dimanche ) }oix;

add_pattern
  m_ord => qr{ (?: 1er | [012]\d | 3[01] | premier | $RE->{number} ) }oix;

add_pattern
  date => qr{ (?:
	      (?:  (?: $RE->{jour}$RE->{comment})? $RE->{m_ord} $RE->{comment})? $RE->{mois} (?: $RE->{comment} (?:[12]\d)?\d\d )?
	      | $RE->{jour} $RE->{comment} $RE->{m_ord} 
	      (?: $RE->{comment} de $RE->{comment} ce $RE->{comment} mois | $RE->{comment} de:prep $RE->{comment} le/det/ $RE->{comment} mois $RE->{comment} (?: prochain | dernier ))?
	    )
	   }oix;


######################################################################
# Titres

add_pattern 
  title => qr{(?: (?:M | Pr | Dr | MM) $RE->{comment} \.)}ox;


######################################################################
# Abbreviations

add_pattern
  abbrev => qr{(?: (?:[A-Z] $RE->{comment}? \.){2,})}ox;


######################################################################
# Telephones

add_pattern
  telsep => qr{(?: $RE->{comment}? [\s.-]+ $RE->{comment}? )}ox;

add_pattern
  telA => qr{ (?: (?:\d{2,} $RE->{telsep}){2,} \d{2,} ) }oix;

add_pattern
  telB => qr{ (?: (?:\d{3,}$RE->{telsep}){1,}\d{3,}) }oix;

add_pattern
  tel_ind => qr{(?: \( $RE->{comment} \d{2,}$RE->{comment}\) 
                  |  \+\d{2,} )}oix;

add_pattern
  tel => qr{(?: (?: (?:Tel|Fax) $RE->{comment}? (?:[:]+$RE->{comment}))?
                (?: $RE->{tel_ind} $RE->{comment})?
                (?: $RE->{telA} | $RE->{telB} )
             )}oix;

######################################################################
# Emails

add_pattern
   email => qr{(?:[\w-]+(?:\.[\w-]+)*\@(?:[\w-]+\.)*\w{2,3})}ox;

1;
