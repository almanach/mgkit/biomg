## To remove some rare Lefff entries

la nc
le nc
La nc
Le nc
feuille v
feuilles v
Feuille v
Feuilles v
est adj

il ilimp
�a caimp
-t-il ilimp
�' caimp

@t�tram�re nc
@pentam�re nc

$ nc

court nc
plus coo
moins coo

@lisse nc

loges v
loge v

@fleur adj

pr�s adj
cf prep
sur adj
correspondant adj

surtout nc
mais nc

@rouge nc adv
@jaune nc adv
@blanc nc adv

@�troit nc
@�troite nc

@clair nc
@brillant nc

@pur nc

@fine nc

lisse v

graine v
graines v

fronde v
frondes v

_NUMBER det

sur adj
portant nc
mais adv

## Special cat for impersonals. Not yet handled by FRMG
il ilimp
�a caimp
-t-il ilimp
-il ilimp
�' caimp
que csu
qu' csu
ce ce
c' ce

## TMP:
a nc
est nc
est  adj
se clar cldr
s' clar cldr

cela caimp
