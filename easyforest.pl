/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2004, 2005 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  easyforest.pl -- Conversion to EASy format
 *
 * ----------------------------------------------------------------
 * Description
 *  Conversion of DyALog dependency forest to expected EASy format
 * ----------------------------------------------------------------
 */

:-import{ module=> xml,
	  file => 'libdyalogxml.pl',
	  preds => [
		    reader/2,
		    stream_reader/3,
		    read_event/3,
		    read_raw_event/2,
		    event_handler/5,
		    event_process/3,
		    event_process/4,
		    event_ctx/2,
		    event_super_handler/2
		   ]
         }.


:-import{ module=> sqlite,
	  file => 'libdyalogsqlite.pl'
	}.

%% To undo potential presence of -parse in compile flags
%% important to be in this mode to parse the list of options to easyforest
:-parse_mode(list).

:-require('format.pl').
:-require('rx.pl').

:-features(ctx,[class,node,constraint,current]).

:-features(cluster, [id,left,right,lex] ).
:-features(node, [id,cluster,cat,tree,lemma,xcat,deriv] ).
:-features(edge, [id,source,target,type,label,deriv] ).

:-features(f,[id,lex,left,right,cid]).
:-features(groupe, [id,type,content,left,right] ).
:-features(relation, [id,type,arg1,arg2,arg3] ).

:-extensional
	cluster{},
	node{},
	edge{},

	f{},
	groupe{},
	sentence/1,
	used/1,
	opt/1
	.

:-rec_prolog relation{}.

:-finite_set(cat,
	     [-,'CS','N','N2','PP','S','V','VMod',
	      adj,adv,advneg,
	      aux,v,
	      cla,cld,clg,cll,cln,clr,clneg,
	      comp,coo,csu,det,
	      nc,np,number,ncpred,
	      poncts,ponctw,
	      prel,prep,pri,pro,conj]).

:-subset( nominal, cat[nc,cln,pro,np,pri,prel,ncpred] ).

:-subset( terminal, cat[~ [-,'CS','N','N2','PP','S','V']] ).

:-finite_set(pos,[left,right,xleft,xright,in,out,xin,xout]).

:-finite_set(const,['GN','GA','GP','NV','GR','PV']).

:-finite_set(rel,[ 'SUJ-V',	% sujet-verbe
		   'AUX-V',	% auxiliaire-verbe
		   'COD-V',	% cod-verbe
		   'CPL-V',	% complement-verbe
		   'MOD-V',	% modifier verbe
		   'COMP',	% complementeur: csu->NV, prep->(GN,NV)
		   'ATB-SO',	% attribut sujet/objet  
		   'MOD-N',	% modifier nom
		   'MOD-A',	% modifier adjectif
		   'MOD-R',	% modifier adverbe
		   'MOD-P',	% modifier preposition
		   'COORD',	% coordination
		   'APPOS',	% apposition
		   'JUXT'	% juxtaposition
		   ]).

:-finite_set(label,[ subject,object,arg,preparg,scomp,comp,xcomp,
		     'Infl','V1','V',v,'vmod',
		     cla,cld,clg,cll,clr,clneg,
		     void,coo,coo2,coord2,coord3,
		     det,det1, incise,en,
		     nc,np,number,ncpred,
		     'CS','Modifier',
		     'N','N2','N2app',
		     'PP',prep,csu,
		     prel,pri,pro,'Root','Punct',
		     'S','S2','SRel','SubS',varg,
		     start,wh,starter,advneg,aux,ce
		     ]).

:-finite_set(edge_kind,[adj,subst,lexical,virtual,epsilon]).

:-xcompiler((xml!wrapper(Handler,Ctx,Name,Attr,G) :-
	     event_process(Handler,start_element{ name => Name, attributes => Attr },Ctx),
	     G,
	     event_process(Handler,end_element{ name => Name },Ctx)
	    )).

:-xcompiler((xml!text(Handler,Ctx,Text) :-
	    event_process(Handler,characters{ value => Text }, Ctx ))).


:-extensional agglutinate/3.

agglutinate('�','le__det','au').
agglutinate('�','les__det','aux').
agglutinate('�','lequel','auquel').
agglutinate('�','lesquels','auxquels').
agglutinate('�','lesquelles','auxquelles').
agglutinate('de','le__det','du').
agglutinate('de','lequel','duquel').
agglutinate('de','lesquels','desquels').
agglutinate('de','lequelles','desquelles').
agglutinate('de','les__det','des').
agglutinate('en','les__det','�s').
agglutinate('�','ledit','audit').
agglutinate('�','lesdits','auxdits').
agglutinate('�','lesdites','audites').
agglutinate('de','ledit','dudit').
agglutinate('de','lesdits','dudits').
agglutinate('de','lesdites','auxdites').

:-std_prolog
	init/0,
	reader/0,
	emit/0,
	convert/0
	.

init :-
	true
	.

reader :-
	stream_reader([],Reader,_),
	event_handler(Reader,loop,eof,[],[])
	.
	    
emit :-
	sentence(Sent),
	name_builder('E~w',[Sent],SId),
	recorded(mode(Mode)),
	event_ctx(Ctx,0),
	Handler=default([]),
	event_process(Handler,start_document,Ctx),
%%	event_process(Handler,pi{name=>xml,value=> [version:'1.0',encoding:'latin1']},Ctx),
	event_process(Handler,xmldecl,Ctx),
	xml!wrapper(Handler,
		    Ctx,
		    'DOCUMENT',
		    [xmlns!xlink= 'http://www.w3.org/1999/xlink'],
		    xml!wrapper(Handler,
				Ctx,
				'E', [ id:SId, mode:Mode ],
				%% Constituants and relations
				event_process(Handler,[constituants,relations],Ctx)
			       )
		   ),
	event_process(Handler,end_document,Ctx)
	.

convert :-
	%% Add a virtual root node
	record( CRoot::cluster{ id => root, lex => '', left => 0, right => 0 } ),
	mutable(MRDerivs,[]),
	every(( N::node{ id => NId, cat => Cat },
		(\+ N = NRoot),
		((   \+ edge{ target => N } ) xor Cat = cat[v,aux] ),
		mutable_read(MRDerivs,_L),
		mutable(MRDerivs,[root(NId)|_L])
	      )),
	mutable_read(MRDerivs,RDerivs),
	record( NRoot::node{ id => root, cluster => CRoot, cat => [], tree => [], deriv => RDerivs }),
	every(( domain(root(NId),RDerivs),
		NN::node{ id => NId },
		verbose('Add root edge ~E\n',[NN]),
		record_without_doublon( edge{ id => root(NId),
					      source => NRoot,
					      target => NN,
					      label => root,
					      type => virtual,
					      deriv => [root(NId)]
					    } ))),
	wait( best_parse(root,_,_,_) ),
	every(( recorded( list_best_parse(root,LBP) ),
%%		verbose('LIST ~w\nsearch ~w ~w ~w\n',[LBP,CIds,EIds,W]),
		domain(best_parses(root,CIds,W,EIds),LBP),
		verbose('Best parse from virtual root:\n\tcids=~w\n\teids=~w\nw=~w\n\n',[CIds,EIds,W]),
		\+ (   _C::cluster{ id => _CId, lex => _Lex },
		    _Lex \== '',
		    \+ ( domain(_CId,CIds)
		       xor cluster_overlap(_C,__C::cluster{ id => __CId }),
			   verbose('Overlap ~E ~E\n',[_C,__C]),
			   domain(__CId,CIds)
		       ),
		    verbose('Pb with ~E\n',[_C])
		) ->
%%		verbose('Found best parse ~w: ~w ~w\n', [W,CIds,EIds]),
		  verbose('Found best parse ~w: ~w ~w~L\n', [W,CIds,EIds,['\n\t~e',''],EIds]),
		  every(( _E::edge{ id => _EId },
			  \+ domain(_EId,EIds),
			  erase( _E ) )),
		  every(( _N::node{},
			  \+ (edge{ source => _N} ; edge{ target => _N }),
			  erase( _N ) )),
		  every(( _C::cluster{ lex => Lex},
			  Lex \== '',
			  \+ node{ cluster => _C },
			  erase(_C) )),
		  (   \+ opt(verbose)
		  xor every(( _C::cluster{}, format('Cluster ~E\n',[_C]) )),
		      every(( _N::node{}, format('Node ~E\n',[_N]) )),
		      every(( _E::edge{ id => _EId },
			      '$answers'(edge_cost(_EId,_W)),
			      format('Edge ~w: ~E\n',[_W,_E]) ))
		  )
	      ;	  find_best_parse_coverage ->
		  (   \+ opt(verbose)
		  xor every(( _C::cluster{}, format('Cluster ~E\n',[_C]) )),
		      every(( _N::node{}, format('Node ~E\n',[_N]) )),
		      every(( _E::edge{ id => _EId },
			      '$answers'(edge_cost(_EId,_W)),
			      format('Edge ~w: ~E\n',[_W,_E]) ))
		  )
	      ;	  
		  verbose('Not found best parse\n', [])
	      )),
	verbose('Starting coord expand\n',[]),
	(coord_expand xor true),
	verbose('Done coord expand\n',[]),
	every(( word(0,1) )),
	verbose('Done word\n',[]),
	every(( domain(Type,['NV','GN','GA','GR','GP','PV']),
		verbose('Try build group ~w\n',[Type]),
		build_group(Type),
		verbose('Done build group ~w\n',[Type]),
		true
	      )),
	verbose('Done group\n',[]),
	every(( extract_relation( relation{ type => rel[] } )))
	.


:-std_prolog clusters_coverage/2.

clusters_coverage(CIds::[CId|_],Length) :-
	cluster{ id => CId, left => Left },
	bound_max(CIds,Left,Right),
	Length is Right - Left
	.


:-std_prolog find_best_parse_coverage/0.

find_best_parse_coverage :-
	verbose('Try find best coverage\n',[]),
%%/*
	every(( N::node{id=>NId},
		best_parse(NId,_,_,_)
	      )),
%%*/
	mutable(M,[]),
	every((
	       '$answers'( O::best_parse(NId,CIds,EIds,W) ),
	       clusters_coverage(CIds,Length),
	       mutable_read(M,_L),
	       %%	       Length > 1,
	       length_sorted_add(Length,O,_L,_LL),
	       mutable(M,_LL)
	      )),
	mutable_read(M,L),
	mutable(M,[]),
	verbose('Sorted partial best parses ~w\n',[L]),
	best_parse_traversal(L,CIds,EIds),
	verbose('Second try: found best parse by gluing ~w ~w\n',[CIds,EIds]),
	/*
	\+ ( cluster{ id => _CId, lex => _Lex },
	       _Lex \== '',
	       \+ domain(_CId,CIds) ),
	*/
	every(( _E::edge{ id => _EId,
			  source => node{ cluster => C1::cluster{ id => CId1}},
			  target => node{ cluster => C2::cluster{ id => CId2}}
			},
		\+ domain(_EId,EIds),
%%		generalized_domain(C1,CIds),
%%		generalized_domain(C2,CIds),
		erase( _E ) )),
	every(( _N::node{},
		\+ (edge{ source => _N} ; edge{ target => _N }),
		erase( _N ) )),
	every(( _C::cluster{ id => _CId, lex => Lex },
		Lex \== '',
		\+ domain(_CId,CIds),
%%		generalized_domain(_C,CIds),
		erase(_C)
	      ))
	.

:-std_prolog generalized_domain/2.

generalized_domain(C::cluster{ id => CId }, CIds ) :-
	(   domain(CId,CIds)
	xor cluster_overlap(C,C2::cluster{ id => CId2 }),
	    domain(CId2,CIds)
	)
	.
			    
	      
:-std_prolog best_parse_traversal/3.

best_parse_traversal( L,
		      CIds,
		      EIds
		    ) :-
	mutable(MCIds,[]),
	mutable(MEIds,[]),
	every(( domain([Length|KL],L),
		domain([W|WL],KL),
		domain(best_parse(NId1,CIds1,EIds1,_),WL),
		mutable_read(MCIds,_CIds),
		mutable_read(MEIds,_EIds),
		cluster!safe_concat(CIds1,_CIds,NewCIds),
		edge_safe_concat(EIds1,_EIds,NewEIds),
		verbose('Best parse recovery: agglutinate length=~w weight=~w node=~w\n',[Length,W,NId1]),
		mutable(MCIds,NewCIds),
		mutable(MEIds,NewEIds)
	      )),
	mutable_read(MCIds,CIds),
	mutable_read(MEIds,EIds)
	.

:-rec_prolog length_sorted_add/4.

length_sorted_add(Length,O::best_parse(_,_,_,W),[],[[Length|WL]]) :-
	weight_sorted_add(W,O,[],WL).
	
length_sorted_add( Length,
		   O::best_parse(_,_,_,W),
		   L::[X::[_Length|WL]|L2],
		   XL
		 ) :-
	( Length == _Length ->
	    XL = [[Length|XWL]|L2],
	    weight_sorted_add(W,O,WL,XWL)
	;   Length > _Length ->
	    XL = [[Length|XWL]|L],
	    weight_sorted_add(W,O,[],XWL)
	;   
	    XL = [X|XL2],
	    length_sorted_add(Length,O,L2,XL2)
	)
	.

:-rec_prolog weight_sorted_add/4.

weight_sorted_add(W,O,[],[[W,O]]).
weight_sorted_add(W,O,L::[X::[_W|KL]|L2],XL) :-
	( W == _W ->
	    XL = [[W,O|KL]|L2]
	;   W > _W ->
	    XL = [[W,O]|L]
	;   
	    XL= [X|XL2],
	    weight_sorted_add(W,O,L2,XL2)
	)
	.

:-prolog (dcg parse_options/0, parse_option/0).

parse_options --> parse_option, parse_options ; [].
parse_option -->
        ( ['-e',Id] -> { record( sentence(Id) )}
	;   ['-verbose'] -> { record_without_doublon( opt(verbose) ) }
	;   ['-restrictions',File] -> {
				       verbose('open restriction database ~w\n',[File]),
				       sqlite!open(File,DB),
				       record_without_doublon( opt(restrictions(DB,File)) )
				      }
	;   { fail }
        )
        .

main :-
	init,
	argv( Argv ),
	phrase( parse_options, Argv, [] ),
	(   sentence(_) xor record( sentence(1) ) ),
	reader,
	(\+ opt(verbose) xor
	    every(( C::cluster{}, format('Cluster ~E\n',[C]) )),
	    every(( N::node{}, format('Node ~E\n',[N]) )),
	    every(( E::edge{}, format('Edge ~E\n',[E]) ))
	),
	wait((convert)),
	emit,
	fail
	.

?- main.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reducing ambiguities

:-light_tabular best_parse/4.
:-mode(best_parse/4,+(+,-,-,-)).

%% best_parse(NodeId,ClusterIds,EdgeIds,Weight)

best_parse(NId,CIds,EIds,W) :-
	N::node{ id => NId, cluster => C::cluster{ id => CId, lex => Lex } },
	verbose('Try find best parse from ~w\n',[NId]),
	best_parse_in(N),
	mutable( M, [] ),
	every((
	       recorded( _BP::best_parses(NId,_CIds,_W,_EIds) ),
	       mutable_read(M,_L),
	       add_best_parse( _BP, _L, _LL),
	       mutable(M, _LL)
	      )),
	mutable_read(M,L),
	record( list_best_parse(NId,L) ),
	domain( best_parses(NId,CIds,W,EIds), L),
	verbose('Found best parse from ~E: ~w ~w ~w\n',[N,W,CIds,EIds]),
	true
	.


:-rec_prolog add_best_parse/3.

add_best_parse(X,[],[X]).
add_best_parse(X::best_parses(_,_,WX,_),L::[Y::best_parses(_,_,WY,_)|L1],LL) :-
	( WX >= WY ->
	    LL = [X|L]
	;   add_best_parse(X,L1,LL1),
	    LL=[Y|LL1]
	)
	.

:-std_prolog best_parse_in/1.

best_parse_in( N::node{ id => NId,
			cluster => C::cluster{ id => CId, lex => Lex },
			deriv => All_Derivs
		      }
	     ) :-
	all_edges(N,All_Edges),
	verbose('All edges from ~E ~w: ~w\n',[N,All_Edges,All_Derivs]),
	every((

	       ( All_Derivs == [] ->
		   Deriv = dummy
	       ;   
		   domain(Deriv,All_Derivs)
	       ),
	       edge_select(N,All_Edges,Deriv,_CIds1,_EIds,_W_Raw,Added),
	       
	       (   Lex \== '',
		   var(Added) ->
		   \+ domain(CId,_CIds1),
		   (   wrapper(N) xor simple_contiguous([CId],_CIds1) ),
		   cluster!safe_concat_aux(C,_CIds1,_CIds)
	       ;   
		   _CIds = _CIds1,
		   (_CIds \== [] xor allowed_empty_parse(N) )
	       ),
	       
	       verbose('Found pre parse from ~E: ~w ~w ~w\n',[N,_W_Raw,_CIds,_EIds]),
	       
	       mutable(_WM,_W_Raw),
	       every(( node_cost_regional(NId,All_Edges,_CIds,_EIds,_W_Reg),
		       mutable_read(_WM,_W1),
		       _W2 is _W1 + _W_Reg,
		       mutable(_WM,_W2) )),
	       mutable_read(_WM,_W),	   
	       
	       verbose('Found parse from ~E: ~w ~w ~w\n',[NId,_W,_CIds,_EIds]),
	       ( recorded( O::best_parses(NId,_CIds,_W1,_) ) ->
		   ( _W1 > _W -> true
		   ;   _W1 < _W,
		       safe_erase(O),
		       %% format('Parse best record ~w ~w ~w\n',[_W,_CIds,_EIds]),
		       record( best_parses(NId,_CIds,_W,_EIds) )
		   ;   _W1 = _W, 
		       random(K), %% Random choice to keep or replace
		       0 is K mod 2,
		       safe_erase(O), 
		       record( best_parses(NId,_CIds,_W,_EIds) )
		   )
	       ;   value_counter(NId,V), V  < 4 ->
		   record( best_parses(NId,_CIds,_W,_EIds) ),
		   update_counter(NId,_)
	       ;   %% Max best parses recorded for NId
		   %% Keep current one only if we can remove a worst one wrt Weight
		   recorded( O2::best_parses(NId,_,_W2,_)),
		   _W2 < _W,
		   \+ ( recorded( best_parses(NId,_,_W3,_)),
			  _W3 < _W2 ) ->
		   safe_erase(O2),
		   record( best_parses(NId,_CIds,_W,_EIds) )
	       ;   %% Max best parses already recorded all better than _W
		   %% do nothing
		   true
	       )))
	.

:-light_tabular all_edges/2.
:-mode(all_edges/2,+(+,-)).

all_edges( N::node{ id => NId, cluster => C::cluster{ id => CId, lex => Lex } },
	   All_Edges
	 ) :-
	%% collect set of all out edges and derivs
	mutable(ME,[]),
	every(( edge{ source => N, id => EId, target => _N::node{ id => _NId} },
		%%		(   best_parse(_NId,_,_,_), fail ; true ),
		mutable_read(ME,_L),
		edge_sorted_add(EId,_L,_LL),
		mutable(ME,_LL)
	      )),
	%% Select subset of all edges
	mutable_read(ME,All_Edges)
	.
	

:-light_tabular allowed_empty_parse/1.

allowed_empty_parse(node{ cat => start, cluster => cluster{ lex => '' }}).
allowed_empty_parse(node{ cat => end, cluster => cluster{ lex => '' }}).

:-light_tabular wrapper/1.

wrapper(node{ cat => incise }).

:-finite_set(quoted,[double_quoted,chevron_quote,simnple_quoted]).

wrapper(node{ tree => Tree }) :- domain(quoted[],Tree).

:-light_tabular edgeid2cluster/2.
:-mode(edgeid2cluster/2,+(+,-)).

edgeid2cluster(EId,Cluster) :-
	edge{ id => EId, target => node{ cluster => Cluster::cluster{} } }.

:-std_prolog edge_safe_concat/3.

edge_safe_concat(L,L2,L4) :-	  
	( L == [] ->
	    L2 = L4
	;   L= [E|L1],
	    edge_safe_concat(L1,L2,L3),
	    edge_safe_add(E,L3,L4)
	)
	.

:-std_prolog edge_safe_add/3.

edge_safe_add(A,L,XL) :-
	( L = [] -> XL = [A]
	;   L=[B|L2],
	    (	A == B -> fail
	    ;	A @< B -> XL = [A|L]
	    ;	XL=[B|XL2],
		edge_safe_add(A,L2,XL2)
	    )
	)
	.

:-rec_prolog edge_sorted_add/3.

edge_sorted_add(EId,[],[EId]).
edge_sorted_add(EId1,L::[EId2|L2],XL) :-
	EId1 \== EId2,
	edgeid2cluster(EId1,cluster{ left => Left1, right => Right1 } ),
	edgeid2cluster(EId2,cluster{ left => Left2, right => Right2 } ),
	(   ( Left1 < Left2
	    xor (Left1 == Left2,
		    (	Right1 < Right2
		    xor Right1 == Right2, EId1 @< EId2 ))) ->
	    XL = [EId1|L]
	;   
	    edge_sorted_add(EId1,L2,XL2),
	    XL = [EId2|XL2]
	)
	.

:-std_prolog safe_erase/1.

safe_erase(G) :-
        every(( recorded(G,Addr),
                delete_address(Addr) ))
        .


%% :-rec_prolog edge_select/5.

%%:-light_tabular edge_select/6.
%%:-mode(edge_select/6,+(+,+,-,-,-,-)).

:-std_prolog edge_select/7.

edge_select(N,
	    Edges,
	    Deriv,
	    CIds,
	    EIds,
	    W,
	    Added
	   ) :-
	verbose('Enter Edge select ~E deriv=~w ~w\n',[N,Deriv,Edges]),
	( Edges = [] ->
	    EIds = [],
	    CIds = [],
	    W = 0
	;
	    Edges = [EId1|Edges2],
	    E1::edge{ id=>EId1,
		      target => N1::node{ id => NId1,
					  cluster => C1::cluster{ left => Left1, right => Right1 }},
		      source => N::node{ id => NId,
					 cluster => C::cluster{ id => CId, lex => Lex, left => Left } },
		      deriv => Derivs1
		    },
	    edge_select(N,Edges2,Deriv,_CIds2,EIds2,W2,Added),
	    (	Lex \== '',
		Right1 =< Left,
		var(Added) ->
		\+ domain(CId,_CIds2),
		Added = yes,
		(   wrapper(N) xor simple_contiguous([CId],_CIds2)),
		cluster!safe_concat_aux(C,_CIds2,CIds2)
	    ;	
		CIds2 = _CIds2
	    ),
	    verbose('Middle Edge select ~E ~w deriv=~w=> ~w ~w\n',[N,Edges,Deriv,CIds2,EIds2]),
	    verbose('Edge examine ~E derivs=~w\n',[E1,Derivs1]),
	    ( domain(Deriv,Derivs1) ->
%%		\+ edge_fall_in(E1,CIds2),
		verbose('Here\n',[]),
		(   %% \+ edge_potential_optional(EId1) xor
		    (	edge_rank(E1,Rank,_), Rank =< 5 )
		xor ( cluster_distance(C,C1,D),
			verbose('Cluster distance ~w\n',[D]),
			(   D =< 12
			xor \+ (   verbose('Look concurrent edge\n',[]),
				   target_pos(Left1,_E1,_D),
				   verbose('Found concurrent edge ~E _d=~w\n',[_E1,_D]),
				   _D < D
			       )
			)
		    )
		xor fail
		),
		verbose('Edge keep ~E d=~w _d=~w deriv=~w\n',[E1,D,_D,Deriv]),
		(   best_parse(NId1,CIds1,_EIds1,W1)
		;   \+ best_parse(NId1,_,_,_),
		    verbose('Using recorded tmp best parses for ~w\n',[NId1]),
		    recorded( best_parses(NId1,CIds1,W1,_EIds1) )
		;   fail,
		    \+ best_parse(NId1,_,_,_),
		    \+ recorded( best_parses(NId1,_,_,_) ),
%%		    CIds1 = [CId1], _EIds1 = [], W1 = 0
		    \+ recorded(locked(NId1)),
		    record_without_doublon(locked(NId1)),
		    verbose('Trying light best parses for ~w\n',[NId1]),
		    best_parse_in(N1),
		    recorded( best_parses(NId1,CIds1,W1,_EIds1) )
		),
		verbose('Retrieved best parse ~w deriv=~w: ~w ~w ~w\n',[NId1,Deriv,W1,CIds1,_EIds1]),
		edge_cost(EId1,WE), W is WE+W1+W2,
		edge_sorted_add(EId1,_EIds1,EIds1),
		verbose('Middle2 Edge select ~E ~w deriv=~w => ~w ~w\n',[N,Edges,Deriv,CIds1,EIds1]),
		(   wrapper(N)
		xor simple_contiguous(CIds1,CIds2)
		%% Next line is for cases of wrapping adjunction with empty left
		xor simple_contiguous(CIds2,CIds1) 
		),
		verbose('Try safe concat ~w ~w\n',[CIds1,CIds2]),
		cluster!safe_concat(CIds1,CIds2,CIds),
		verbose('Success safe concat ~w ~w => ~w\n',[CIds1,CIds2,CIds]),
		edge_safe_concat(EIds1,EIds2,EIds)
	    ;	/* No need to test when using derivations
		edge_potential_optional(EId1),
		(   cluster_distance(C,C1,D) > 12
		xor (   verbose('Look concurrent edge\n',[]),
			target_pos(Left1,_E1,_D),
			verbose('Found concurrent edge ~E _d=~w\n',[_E1,_D]),
			_D =< 12
		    )
		xor fail
		),
		*/
		verbose('Edge discard ~E\n',[E1]),
		W = W2,
		CIds = CIds2,
		EIds = EIds2
	    ),
	    ( var(Added) -> \+ domain(CId,CIds) ; true ),
	    true
	),	
	verbose('Return Edge select ~E ~w deriv=~w added=~w=> ~w ~w ~w\n',[N,Edges,Deriv,Added,W,CIds,EIds]),
	true
	.

%%:-light_tabular cluster_distance/3.
%%:-mode(cluster_distance/3,+(-,-,-)).

:-light_tabular target_pos/3.
:-mode(target_pos/3,+(+,-,-)).

target_pos(Pos,E,D) :-
	E::edge{ target => node{ cluster => C1::cluster{ left => Pos }},
		 source => node{ cluster => C::cluster{}}},
	cluster_distance(C,C1,D)
	.

:-light_tabular edge_rank/3.
:-mode(edge_rank/3,+(+,-,-)).

edge_rank( E::edge{ source => N,
		    target => T::node{ cluster => cluster{ left => Left, right => Right } }
		  },
	   Rank,
	   Dir
	 ) :-
	(   edge_to_right(E) ->
	    Dir = right,
	    (	E1::edge{ source => N,
			  target => T1::node{ cluster => cluster{ left => Left1,
								  right => Right1
								}
					    }},
		E1 \== E,
		edge_to_right(E1),
		Right1 =< Left,
		\+ ( edge{ source => N,
			   target => T2::node{ cluster => cluster{ left => Left2,
								   right => Right2
								 }
					     }},
		       Right2 =< Left,
		       Right1 =< Left2
		   )
	    ->	
		edge_rank(E1,Rank1,right),
		Rank is Rank1 + 1
	    ;	
		Rank is 1
	    )
	;   
	    Dir = left,
	    (	E1::edge{ source => N,
			  target => T1::node{ cluster => cluster{ left => Left1,
								  right => Right1
								}
					    }},
		E1 \== E,
		\+ edge_to_right(E1),
		Right =< Left1,
		\+ ( edge{ source => N,
			   target => T2::node{ cluster => cluster{ left => Left2,
								   right => Right2
								 }
					     }},
		       Right =< Left2,
		       Right2 =< Left1
		   ) ->  
		edge_rank(E1,Rank1,left),
		Rank is Rank1 + 1
	    ;	
		Rank is 1
	    )
	),
	verbose('Edge rank ~w ~w ~E\n',[Rank,Dir,E]),
	true
	.

:-std_prolog cluster_distance/3.

cluster_distance(C1::cluster{ left => L1, right => R1 }, C2::cluster{ left => L2, right => R2 },D) :-
	( R1 =< L2 ->
	    D is L2 - R1
	;
	    D is L1 - R2
	)
	.

:-rec_prolog simple_contiguous/2.

simple_contiguous([],_).
simple_contiguous(_,[]).
simple_contiguous([CId1|L2],L::[CId|_]) :-
	( L2 = [] ->
	    contiguous(CId1,CId)
	;   L2 = [CId2|L3],
	    contiguous(CId1,CId2) ->
	    simple_contiguous(L2,L)
	;   contiguous(CId1,CId),
	    simple_contiguous(L,L2)
	)
	.

:-light_tabular contiguous/2.

contiguous(CId1,CId2) :-
	cluster{ id => CId1, right => Pos },
	cluster{ id => CId2, left => Pos }
	.

:-std_prolog edge_fall_in/2.

edge_fall_in( E::edge{ target => N::node{ cluster => cluster{ id => CId, lex => Lex } } },
	      CIds
	    ) :-
	verbose('Try edge fall in ~E cids=~w\n',[E,CIds]),
	( domain(CId,CIds) ->
	    true
	;   
	    Lex == '',
	    \+ ( E2::edge{ source => N, target => node{ cluster => cluster{ id => CId2, lex => Lex2 } } },
		   verbose('Here ~E lex2=~w\n',[E2,Lex2]),
%%		   Lex2 \== '',
		   verbose('Here2 ~E\n',[E2]),
		   \+  edge_fall_in(E2,CIds))
	)
	.


:-light_tabular edge_to_right/1.

edge_to_right( edge{ id => EId,
		     source => node{ cluster => cluster{ right => Right } },
		     target => node{ cluster => cluster{ left => Left } }
		   } ) :-
	Right =< Left
	.

:-rec_prolog bound_max/3.

bound_max([],Boundary,Boundary).
bound_max([CId|CIds],Boundary,NewBoundary) :-
	(   cluster{ id => CId, left => Boundary, right => Right } ->
	    bound_max(CIds,Right,NewBoundary)
	;
	    NewBoundary = Boundary
	)
	.

:-light_tabular cluster_overlap/2.

cluster_overlap( C1::cluster{ lex => Lex1, id => CId1, left => Left1, right => Right1 },
		 C2::cluster{ lex => Lex2, id => CId2, left => Left2, right => Right2 }
	       ) :-
	C1, Lex1 \== '',
	C2, Lex2 \== '',
	(   CId1 == CId2
	xor Left1 =< Left2, Left2 < Right1
	xor Left2 =< Left1, Left1 < Right2
	)
	.

:-light_tabular edge_potential_optional/1.

edge_potential_optional(EId) :-
	edge{ id => EId, target => node{ cluster => C::cluster{ id => CId }}},
	edge{ id => _EId, target => node{ cluster => _C::cluster{ id => _CId }}},
	_EId \== EId,
	cluster_overlap(C,_C)
	.

edge_potential_optional(EId) :-
	edge{ id => EId,
	      target => N::node{ cluster => cluster{ lex => '', left => Left }}
	    },
	edge{ id => EId1,
	      source => N,
	      target => node{ cluster => C::cluster{ left => Left }}
	    },
	edge_potential_optional(EId1)
	.

edge_potential_optional(root(_)). %% Virtual nodes

:-std_prolog cluster!safe_concat/3.

cluster!safe_concat(L,L2,L4) :-	  
	( L == [] ->
	    L2 = L4
	;   L= [A|L1],
	    cluster!safe_concat(L1,L2,L3),
	    C::cluster{ id => A },
	    cluster!safe_concat_aux(C,L3,L4)
	)
	.

:-std_prolog cluster!safe_concat_aux/3.

cluster!safe_concat_aux( CA::cluster{ id => A, left => LeftA, right => RightA },
			 L1,
			 XL
		       ) :-
	( L1 == [] ->
	    XL = [A] 
	;   L1 = [B|L2],
	    CB::cluster{ id => B, left => LeftB, right => RightB },
	    ( cluster_overlap(CA,CB) -> fail
	    ;	RightA =< LeftB ->
		XL = [A|L1]
	    ;	cluster!safe_concat_aux(CA,L2,XL2),
		XL = [B|XL2]
	    )
	)
	.


:-light_tabular edge_cost/2.
:-mode(edge_cost/2,+(+,-)).

edge_cost(EId,W) :-
	Edge::edge{ id => EId, source => node{}, target => node{} },
	mutable(WM,0),
	every(( edge_cost_elem(Name,Edge,_W),
		verbose('Edge cost ~w ~w ~E\n',[Name,_W,Edge]),
		mutable_read(WM,_W1),
		_W2 is _W1 + _W,
		mutable(WM,_W2)
	      )),
	mutable_read(WM,W),
	verbose('TOTAL Edge cost ~w ~E\n',[W,Edge]),
	true
	.

:-rec_prolog edge_cost_elem/3.

%% Penalty for adj dependencies
%% edge_cost_elem( edge{ type => adj }, -1 ).

%% Favor subst and lexical dependencies
edge_cost_elem( '+SUBST', edge{ type => subst , source => node{ cluster => cluster{ lex => Lex }}}, 10 ) :-
	Lex \== ''.
edge_cost_elem( '+LEXICAL', edge{ type => lexical , source => node{ cluster => cluster{ lex => Lex }}}, 20 ) :-
	Lex \== ''.

%% Favor ncpred
edge_cost_elem( '+NCPRED', edge{ type => lexical, source => node{ cat => v }, target => node{ cat => ncpred } }, 800).

%% Favor ncpred mod
edge_cost_elem( '+NCPREDMOD',
		edge{ type => adj,
		      label => ncpred,
		      source => node{ cat => v },
		      target => node{ cat => adv}
		    }, 30).


%% Favor verbal argument
edge_cost_elem( '+ARG', edge{ label => label[object,preparg,comp,xcomp] }, 700 ).
edge_cost_elem( '+SUBJ', edge{ label => label[subject] }, 800 ).

%% Favor cleft constructions
edge_cost_elem( '+CLEFT', edge{ label => 'CleftQue' }, 800 ).

%% Penalty for long distance dependencies, except for root node
edge_cost_elem( '-LONG',
		Edge::edge{ source => node{ id => IdA, cluster => cluster{ left => LA, right => RA }},
			    target => node{ cluster => cluster{ left => LB, right => RB }},
			    type => edge_kind[~virtual]
			  },
		D
	      ) :-
	verbose('Try  length penalty on ~E\n',[Edge]),
	( RA =< LB -> 
	    D1 is  (LB - RA)
	;   
	    D1 is (LA - RB)
	),
	D is -20 * max(0,D1-3) 
	.

%% Favour adj attribute rather than noun
edge_cost_elem( '+ATTR', edge{ label => comp, target => node{ cat => adj } }, 100 ).

%% Favour det rather than adj or noun or verb
%% edge_cost_elem( edge{ target => node{ cat => det } }, 40 ).


%% Favour noun over verb in first position, overcoming penalty for virtual edges
edge_cost_elem( '+NOUN/VERB',
		E::edge{ target => node{ cat => nc, cluster => C::cluster{ left => 0 } } }, 1700 ) :-
%%	format('Try Activation ~w\n',[E]),
	edge{ target => node{ cat => v, cluster => C } },
%%	format('Activation\n',[]),
	true
	.

%% Penalty for raw pronon 'ce'
edge_cost_elem( '-CE',
		edge{ target => N::node{ cluster => cluster{ lex => Lex }, cat => pro } },
		-50
	      ) :-
	\+ edge{ source => N },
	label2lex(Lex,[Ce],_),
	domain(Ce,[ce,'Ce'])
	.

%% Favour coordinations
edge_cost_elem( '+COORD', edge{ label => label[coo,coo2,coord2,coord3] }, 20).

%% Penaltie on transcategorization from adj to nc
edge_cost_elem( '-ADJ', edge{ label => 'N2', target => node{ cat => adj } }, -100 ).

%% Favour det preceding numbers
edge_cost_elem( '+NUM.DET', edge{ source => node{ cat => number }, target => node{ cat => det } }, 20).

%% Favour aux-v rather rather than v-acomp
edge_cost_elem( '+AUX-V', edge{ source => node{ cat => v }, target => node{ cat => aux } }, 50 ).

%% Penalty on person constructions (vs e.g participiales): Jean, viens manger !
edge_cost_elem( '-PERS', edge{ target => node{ cat => 'S', tree => Tree } }, -20 ) :-
	domain(person_on_s,Tree)
	.

%% favour genitive over locative
edge_cost_elem( '+CLG', edge{ target => node{ cat => clg }, label => clg }, 50 ).

%% Favour auxiliary over verbs
edge_cost_elem( '+AUX/V', edge{ target => node{ cat => aux }}, 1000 ).

%% Favour 'est' as verb
%% edge_cost_elem( edge{ target => node{ cluster => cluster{ }}, 1000 ).

%% Penalties on filler in robust parsing
edge_cost_elem( '-UNK', edge{ target => node{ cat => unknown } }, -10000).

%% Penalties on virtual edges
edge_cost_elem( '-VIRTUAL', edge{ type => virtual }, -1000).

%% But less penalties on virtual edges leading to verbs
edge_cost_elem( '+VIR->V', edge{ type => virtual, target => node{ cat => v } }, 500 ).

%% Penalties on virtual edges leading to empty short sentence
edge_cost_elem( '-SHORTVIRT', edge{ type => virtual,
		      target => node{ cat => 'S',
				      cluster => cluster{ lex => '' }}
		    },
		-500).

%% Favour closed categories interpretations over others
edge_cost_elem( '+CLOSED',
		edge{ target => node{ cat => cat[prep,det,pri,pro,prel,conj,coo,cln,cla,clr,clg], cluster => C } }, 300 ).
%%	edge{ target => node{ cat => cat[nc,np,adj,adv,v], cluster => C } }.

%% Penalties on sequence of Nc
edge_cost_elem( '-NcSeq',
		Edge::edge{ source => node{ cat => nc },
		      target => node{ cat => nc },
		      label => 'Nc2' },
		-2000 ) :-
	verbose('I am here ~E\n',[Edge])
	.


%% Penalties on use of comma as sentence separator
edge_cost_elem( '-COMASEP',
		Edge::edge{ type => adj, target => node{ cat => 'S', tree => Tree }}, - 2000) :-
	domain(comma_sep,Tree)
	.


%% Penalties on use of comma in enumerations
%% depends on presence of ending ... and number of ','
edge_cost_elem( '-COMAENUM',
		Edge::edge{ type => adj, target => N::node{ tree => Tree }},
		K
	      ) :-
	domain('N2_enum',Tree),
	mutable(M,300),
	every(( edge{ source => N,
		      type => lexical,
		      target => node{ cluster => cluster{ lex => Lex }}},
		label2lex(Lex,[X],_),
		mutable_read(M,_K),
		( X == (',') ->
		    New_K is 2*_K
		;   X == '...' ->
		    New_K is 3*_K
		;
		    New_K is _K
		),
		mutable(M,New_K)
	      )),
	mutable_read(M,_K)
	.

%% Favour adv->[adv,adj] edges
edge_cost_elem( '+ADVMOD',
		edge{ source => node{ cat => cat[adv,advneg,adj] },
		      target => node{ cat => adv },
		      type => adj
		    },
		100 ).


%% Penalties on edge coming from empty cluster
edge_cost_elem( '-EMPTY',
		Edge::edge{ source => node{ cluster => cluster{ lex => '' }}}, -100) :-
	verbose('Empty cluster penalty ~E\n',[Edge])
	.


%% Favour long clusters
edge_cost_elem( '+LONGLEX',
		Edge::edge{ target => node{ cluster => cluster{ lex => Lex, left => Left, right => Right }}},
		K
	      ) :-
	Lex \== '',
	K is (Right - Left - 1) * 100,
	K > 0
	.

%% Favour long clusters case 2
edge_cost_elem( '+LONGLEX2',
		Edge::edge{ target => node{ cluster => cluster{ lex => Lex, left => Left, right => Right }}},
		K
	      ) :-
	Lex \== '',
	label2lex(Lex,L::[_,_|_],_),
	length(L,N),
	K is (N - 1) * 100,
	K > 0
	.

%% Favour prep->X edges
edge_cost_elem( '+PREP->X',
		Edge::edge{ source => node{ cat => prep }, type => edge_kind[subst,lexical] }, 400).

%% Favour lexical v->prep edges
edge_cost_elem( '+V->PREP',
		Edge::edge{ source => node{ cat => v },
			    target => node{ cat => prep },
			    type => edge_kind[lexical] },
		800
	      ).

%% Penalty on v->prep[de,d'] edges
edge_cost_elem( '-V->de',
		Edge::edge{ source => node{ cat => cat['S','VMod'] },
			    target => N::node{ cat => prep, cluster => cluster{ lex => Lex } }
			  },
		- 50 ) :-
	\+ edge{ source => N, target => node{ cat => v } },
	label2lex(Lex,[Lex2],_),
	domain(Lex2,[de,'d'''])
	.

%% Penalty based on rank
edge_cost_elem( '-RANK',
		Edge::edge{}, D ) :-
	edge_rank(Edge,Rank,_),
	D is -5 * Rank.


%% Favor 'est' as verb or aux
edge_cost_elem( '+est/v_or_aux',edge{ source => node{ cat => cat[v,aux],
				      cluster => cluster{ lex => Lex } } },
		200 ) :-
	label2lex(Lex,[est],_)
	.


%% Use of restriction database: PP attachment on verbs
edge_cost_elem( '+RESTR_PP_V',
		edge{ source => node{ lemma => Source, cat => v },
		      target => VMod::node{ cat => 'VMod' },
		      label => vmod,
		      type => adj
		    },
		300) :-
	recorded(opt(restrictions(DB,_))),
	edge{ source => VMod,
	      target => N::node{ cat => prep, lemma => Prep }
	    },
	edge{ source => N,
	      type => subst,
	      target => node{ lemma => Target } },
	check_restriction(Source,Target,Prep)
	.

%% Favor 'en' for gerundives (participiale)
edge_cost_elem( '+en_as_GERUND',
		edge{ source => N::node{ cat => 'S' },
		      target => N2::node{ lemma => 'en' },
		      type => lexical
		    },
		200		% need to compensate penalty for empty source
	      ) :-
	edge{ source => N, target => node{ cat => v }, type => subst },
	verbose('Found gerundive ~E\n',[N2]),
	true
	.

%% Use of restriction database: PP attachment on nouns
edge_cost_elem( '+RESTR_PP_N',
		edge{ source => node{ lemma => Source, cat => nc },
		      target => N::node{ cat => prep, lemma => Prep },
		      label => 'N2',
		      type => adj
		    },
		300) :-
	recorded(opt(restrictions(DB,_))),
	edge{ source => N,
	      type => subst,
	      target => node{ lemma => Target }
	    },
	check_restriction(Source,Target,Prep)
	.

:-light_tabular prepare_restriction/1.

prepare_restriction(PStmt) :-
	recorded( opt(restrictions(DB,_)) ),
	sqlite!prepare(DB,'select tag from restrictions where source=? and target=? and dir=? and rel=?',PStmt)
	.

:-std_prolog check_restriction/3.

check_restriction(Source,Target,Rel) :-
	prepare_restriction(PStmt),
	(   Dir = up, L = [Source,Target,Dir,Rel]
	;   Dir = down, L = [Target,Source,Dir,Rel]
	),
	sqlite!reset(PStmt),
%%	format('Bind ~w\n',[L]),
	sqlite!bind(PStmt,L),
	(   sqlite!tuple(PStmt,[Tag]) xor fail),
	verbose('Restriction found for source=~w target=~w rel=~w dir=~w => ~w\n',[Source,Target,Rel,Dir,Tag])
	.
	
:-rec_prolog node_cost_regional/5.

/*
%% strong penalty for discontiguous parses associated to removed edges
node_cost_regional(NId,All_Edges,CIds,EIds,-1000) :-
	\+ contiguous(CIds),
	verbose('NOT CONTIGUOUS ~w\n',[CIds]),
	true
	.
*/

/*
node_cost_regional(NId,All_Edges,CIds,EIds,-100) :-
	domain(EId1,All_Edges),
	\+ domain(EId1,EIds),
	edge{ id => EId1,
	      target => node{ cluster => cluster{ left => CId1_Left,
						  right => CId1_Right,
						  lex => CId1_Lex }}
	    },

	CId1_Lex \== '',
	(   domain( CLeft, CIds ),
	    cluster{ id => CLeft, left => CLeft_Left },
	    CLeft_Left < CId1_Left,
	    
	    domain( CRight, CIds ),
	    cluster{ id => CRight, right => CRight_Right },
	    CRight_Right > CId1_Right
	->
%%	    verbose('DISCONTIGUOUS SEGMENT ~w [~w]: ~w\n',[NId,EId1:NId1,CIds]),
	    true
	;   
	    fail
	)
	.
*/

/* No need when using derivations
%% Error for loosing a det
node_cost_regional(NId,All_Edges,CIds,EIds,-1000) :-
%%	verbose('Try regional cost ~w\n',[NId]),
%%	edge{ source => NId, id => EId },
	edge{ source => node{ id => NId, cat => cat[nc,adj,np] },
	      target=> node{ cat => cat[number,det,prep] },
	      type => subst,
	      id => EId
	    },
%%	verbose('Try regional cost det ~w\n',[NId]),
	\+ domain(EId,EIds),
%%	verbose('Found lost det: ~w => ~w ~w\n',[NId,CIds,EIds]),
	true
	.
*/

/* No need with derivations
%% Strong penalty for verb with two same kind of args (except for inverted clitic subject)
node_cost_regional(NId,All_Edges,CIds,EIds,-2000) :-
	edge{ id => EId1,
	      source => node{ id => NId, cat => v },
	      label => L::label[subject,object,comp,preparg],
	      target => N1::node{ cat => Cat1 }
	    },
	edge{ id => EId2,
	      source => node{ id => NId, cat => v },
	      label => L,
	      target => N2::node{ cat => Cat2 }
	    },
	EId1 \== EId2,
	domain(EId1,EIds),
	domain(EId2,EIds),
	( L == subject ->
	    \+ domain(cln,[Cat1,Cat2])
	;   L == preparg ->
	    edge{ source => N1, target => XN1::node{ cat => XCat1 }, type => subst },
	    edge{ source => N2, target => XN2::node{ cat => XCat2 }, type => subst },
	    (	XCat1 = XCat2, XCat1 = cat[v,adj] )
	;   
	    true
	)
	.
*/

%% Strong penalty for verbs with subject after cod (when cod after verb)
node_cost_regional(NId,All_Edges,CIds,EIds,-5000) :-
	E1::edge{ id => EId1,
	      source => N::node{ id => NId, cat => v, cluster => cluster{ right => Pos} },
	      target => node{ cluster => cluster{ left => Left } },
	      label => subject
	    },
	E2::edge{ id => EId2,
		  source => N::node{ id => NId, cat => v },
		  target => node{ cluster => cluster{ right => Right } },
		  label => label[object,xcomp]
		},
	domain(EId1,EIds),
	domain(EId2,EIds),
	Pos =< Right,
	Right =< Left,
	verbose('Regional penalty cod before subj ~w ~w\n',[E1,E2]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Expanding coordinations

:-light_tabular coord_expand/0.

coord_expand :-
%%	format('Start coord expand\n',[]),
	every((
	       edge{ source => N1,
		     target => N2::node{ cat => coo },
		     type => adj
		   },
	       %%	       format('Found Coord ~w from ~w\n',[N2,N1]),
	       E3::edge{ source => N3,
			 target => N1,
			 label => L3,
			 type => T3 },
	       record( toerase(E3) ),
	       domain(L,[coord2,coord3]),
	       edge{ source => N2,
		     target => N4,
		     label => L,
		     type => subst,
		     deriv => Deriv % TO BE CHECKED
		     },
	       (   fail, 	% No longer pertinent with new EASy rules
		   \+ edge{ source => N3, target => N4, type => T3, label => L3 },
		   gensym(EId),
		   %%	       format('EXPAND ~w => ~w ~w\n',[EId,N3,N4] ),
		   record_without_doublon( E::edge{ id => EId,
						    source => N3,
						    target => N4,
						    type => T3,
						    label => L3,
						    deriv => Deriv
						  } )
	       ;   %% Reroot entering dependencies to N2 (new EASy rules)
		   \+ edge{ source => N3, target => N2, type => T3, label => L3 },
		   gensym(EId),
		   %%	       format('EXPAND ~w => ~w ~w\n',[EId,N3,N4] ),
		   record_without_doublon( edge{ id => EId,
						 source => N3,
						 target => N2,
						 type => T3,
						 label => L3,
						 deriv => Deriv
					       } )
	       ),
%%	       format('COORD ADDED ~E\n',[E]),
	       true
	      )),
	every(( recorded(toerase(_E)),
		erase(_E) )),
%%	format('End coord expand\n',[]),
	true
	.

coord_expand :-
%%	format('Start coord expand\n',[]),
	every((
	       edge{ source => N1,
		     target => N2::node{ tree => Tree },
		     type => adj
		   },
	       domain('N2_enum',Tree),
	       %%	       format('Found Coord ~w from ~w\n',[N2,N1]),
	       edge{ source => N3,
		     target => N1,
		     label => L3,
		     type => T3 },
	       domain(L,[coord]),
	       edge{ source => N2,
		     target => N4,
		     label => L,
		     type => subst },
	       \+ edge{ source => N3, target => N4, type => T3, label => L3 },
	       gensym(EId),
	       %%	       format('EXPAND ~w => ~w ~w\n',[EId,N3,N4] ),
	       record_without_doublon( E::edge{ id => EId,
						source => N3,
						target => N4,
						type => T3,
						label => L3 } ),
%%	       format('COORD ADDED ~E\n',[E]),
	       true
	      )),
%%	format('End coord expand\n',[]),
	true
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Building words

:-std_prolog word/2.

word(_Left,Delta) :-
	(   C::cluster{ id=> Id, left => Left, right => Right, lex => Lex },
	    \+ recorded( redirect(Id,_) ),
	    _Left =< Left,
	    Lex \== '',
	    \+ ( cluster{ id => _Id, left => __Left, right => __Right, lex => __Lex },
		   \+ recorded( redirect(_Id,_) ),
		   __Lex \== '',
		   (   (__Left == Left, __Right < Right)
		   xor ( _Left =< __Left, __Left < Left)
		   )
	       ) ->
	    (	C2::cluster{ id => Id2, left => Right, right => Right2, lex => Lex2 },
		agglutinate(Lex,Lex2,AggLex) ->
		record_without_doublon( redirect(Id2,Id) ),
		word_aux([AggLex],Left,Delta,C)
	    ;	
		rx!tokenize(Lex,' ',Words),
		word_aux(Words,Left,Delta,C),
%%		format('Handling ~w => ~w\n',[C,Words]),
		true
	    )
	;   
	    true
	)
	.

:-rec_prolog word_aux/4.

word_aux([],_Pos,Delta,Cluster::cluster{ left => Left , right => Right }) :-
%%	format('Restart word ~w ~w\n',[Pos,Delta]),
	( Right is Left + 1 ->
	    Pos = Right
	;
	    Pos = _Pos
	),
	word(Pos,Delta).

word_aux([Lex|Words],Left,Delta,Cluster::cluster{ id => CId, left => CLeft, right => CRight}) :-
	(   CRight is  CLeft + 1,
	    Words = [_|_] ->
	    XRight = CRight,
	    XLeft = CLeft,
	    Right is Left
	;   
	    XRight = Right,
	    XLeft = Left,
	    Right is Left + 1
	),
	sentence(Sent),
	Seed is Left + Delta,

	(   rx!tokenize(Lex,'|',[Label1,_Lex1|_R]),
	    Label1 \== ''
	->  
	    %% Pb when second '|' arise after first '|'
	    ( _R = [] -> Lex1 = _Lex1 ; Lex1 = Lex ),
%%	    name_builder('E~w~w',[Sent,Label1],Label),
	    Label = Label1,
	    ( Lex1 == '*' ->
		recorded(f{id=> Label, cid => CId0}),
		record_without_doublon( redirect(CId,CId0) )
	    ;	recorded(f{id=> Label, cid => CId0}) ->
		record_without_doublon( redirect(CId,CId0) )
	    ;
		verbose('Register f ~w ~w ~w ~w ~E\n',[Label,Lex1,XLeft,XRight,Cluster]),
		record(f{id=>Label, lex=>Lex1, left=> XLeft, right => XRight, cid => CId})
	    )
	;
	    Lex1 = Lex,
	    update_counter(fids,BetterSeed),
	    name_builder('E~wF~w',[Sent,BetterSeed],Label),
	    record(f{id=>Label, lex=>Lex1, left=> Left, right => Right, cid => CId})
	),

%%	format('Register word ~w: lex=~w left=~w right=~w cluster=~w\n',[Label,Lex,Left,Right,CId]),
	word_aux(Words,Right,Delta,Cluster),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Building constitutants

:-light_tabular constituant/3.

constituant(Left,Right,Const) :-
	'$answers'( Const::group(Type,Left,Right,_,_) ),
	\+ used(Const)
	.

constituant(Left,Right,Const) :-
	Const::cluster{ id => Label, left => Left, right => Right, lex => Lex },
	Lex \== '',
	\+ used(Label)
	.

:-std_prolog x_command/2.

x_command(N1,N2) :-
	(   edge{ source => N1, target => N2 }
	xor edge{ source => N3, target => N1 },
	    edge{ source => N3, target => N2 }
	)
	.

:-light_tabular build_group/1.

build_group(Type) :-
%%	format('Try build group ~w\n',[Type]),
	group(Type,N::node{ cluster => Cluster::cluster{ id => WId }},Nodes),
	verbose('Potential group ~w ~w ~w\n',[Type,N,Nodes]),
	\+ used(WId),
	\+ recorded( redirect( WId, _) ), % Needed to avoid void group
	( Nodes = group(_,Left1,Right,_,Content1) ->
	    Cluster = cluster{ left => Left, right => Left1 },
	    ( domain(WId,Content1) -> Content = Content1 ; Content = [WId|Content1]),
	    mark_as_used([Nodes])
	;   
	    node!add(N,Nodes,Nodes2),
	    node!add_fillers(Nodes2,Nodes3),
	    node!group(Nodes3,Content,Left,Right)
	),
	Left \== Right,
	group(Type,Left,Right,N,Content)
	.

:-light_tabular group/5.
:-mode(group/5,+(+,+,+,+,+)).

group(Type,Left,Right,N,Content) :-
	verbose('GROUP ~w ~w ~w ~E ~w\n',[Type,Left,Right,N,Content]),
	mark_as_used(Content)
	.

:-rec_prolog group/3.

group('NV',N,Nodes) :-
	N::node{ cat => Cat::cat[v,aux] },
	\+ single_past_participiale(N),
	node!first_v_ancestor(N,P1),
	node!collect( (N1::node{ cat => Cat1}) ^
		    (
			node!terminal(N,cat[v,aux],['Infl','V']),
			node!terminal(P1,cat[v],['Infl','V']),
			%% N is first aux or v
			%% get subject of older ancestor P or from N (post subject clitic)
			node!older_ancestor(N,cat[v,aux],P,['Infl','V']),
			record_without_doublon( terminal_v(P,N) ), %% for the SUJ-V relation
%%			format('OLD ANCESTOR ~w => ~w\n',[N,P]),
			Cat1 = cln,
			(   node!dependency(out,P,N1,_)
			;   N \== P, node!dependency(out,N,N1,_)
			)
		    ;	
			node!terminal(N,cat[aux],['Infl','V']),
			%% N is first aux or v in a local chain
			%% (may be preceded by a modal verb)
			%% get all clitics from first v ancestor
			(   Cat1 = cat[clg,cla,cld,cll,clr,clneg,advneg,adv],
			    node!dependency(out,P1,N2::node{ cat => Cat1 },[E])
			;   
			    N \== P1,
			    Cat1 = cat[adv,advneg],
			    node!dependency(out,N,N2,[E])
			),
			%% only keep adv or advneg if left of N and find all dep. from advneg
			%% and only if attached to v (not to S) or some advneg
			%% and preceded by a clneg !
			( Cat1 = cat[adv,advneg] ->
%%			    format('ADV* ~w ~w\n',[N2,E]),
			    E = edge{ label => cat[v,advneg] },
			    edge{ source => N, target => node{ cat => clneg } },
			    node!neighbour(xleft,N,N2),
			    (	N1 = N2 ;
				node!safe_dependency(xout,N2,N1),
%%				format('NV XOUT cat=~w N=~w P1=~w N2=~w -> N1=~w\n',[Cat1,N,P1,N2,N1]),
				true
			    )
			;
			    N1 = N2
			)
		    ),
		      Nodes )
	.

group('GN',N,Nodes) :-
	N::node{ cat => Cat::cat[adj,nc,pro,np,pri,prel,ncpred] },
	( Cat = adj ->
	    edge{ source => N, type => subst, label => det }
	;   Cat = cat[pri,prel] ->
	    \+ edge{ source => node{ cat => 'S'}, target => N },
	    \+ edge{ target => N, label => 'CleftQue' }
	;
	    true
	),
%%	\+ node!neighbour(right,N,node{cat=>cat[nc,np]}),
	node!collect( N1^( node!safe_dependency(xout,N,N1),
%%			   format('GN XOUT ~w -> ~w\n\tL=~L\n',[N,N1,['~E',' '],[]]),
			   node!neighbour(xleft,N,N1) )
	       , Nodes )
	.

group('GP',N,Nodes) :-
	edge{ source => node{ cat => 'S'}, target => N::node{ cat => Cat::cat[prel] }},
	node!collect( N1^( node!safe_dependency(xout,N,N1),
			   node!neighbour(xleft,N,N1) )
		    , Nodes )
	.

group('GP',N,Group1) :-
	'$answers'(Group1::group(const['GN','GR'],Left1,_,Head1,_)),
	node!neighbour(left,Left1,N::node{ cat => prep }),
	verbose('TRY GP ~E ~w ~E\n',[N,Left1,Head1]),
	x_command(N,Head1)
	.

group('PV',N,Group1) :-
	'$answers'(Group1::group('NV',Left1,_,Head1,_)),
	node!neighbour(left,Left1,N),
	(   N = node{ cat => prep } ; N = node{ cat => '_', lemma => en } ),
	x_command(N,Head1)
	.

group('GR',N,[]) :-
	N::node{ cat => cat[adv,advneg] }
	.

group('GR',N,[]) :-
	edge{ target => N::node{ cat => cat[csu] },
	      label => advneg,
	      source => node{ cat => cat[v,aux]}
	    }
	.


group('GR',N::node{ cat => pri },[]) :-
	edge{ source => node{ cat => 'S'}, target => N }
	.


group('GA',N,[]) :-
	N::node{ cat => adj }
	.

group('GA',N,[]) :-
	%% Participiales on nouns, non gerundive (ie. past participle)
	single_past_participiale(N)
	.

:-light_tabular single_past_participiale/1.

single_past_participiale(N::node{}) :-
	edge{ source => node{ cat => 'N2' },
	      target => N::node{ cat => v, cluster => cluster{ lex => Lex } },
	      type => subst,
	      label => 'SubS'
	    },
	\+ edge{ source => N, label => label[object,arg,preparg,scomp,xcomp] },
	\+ edge{ source => N, target => node{ cat => aux } },
%%	format('Try1 participale2adj on ~w ~w\n',[Lex,TLex]),
	(label2lex(Lex,[TLex],_) xor TLex = Lex),
	verbose('Try2 participale2adj on ~w ~w\n',[Lex,TLex]),
	(   rx!match{ string => TLex, rx => rx{ pattern => 'ant$'} } ->
	    verbose('Match succeeded\n',[]),
	    fail
	;
	    verbose('Match failed\n',[]),
	    true
	)
	.
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Extracting dependencies

:-std_prolog extract_relation/1.

extract_relation( R ) :-
	R::relation{ type => Type, arg1 => Arg1, arg2 => Arg2, arg3 => Arg3 },
	arg2id(Arg1,Id1),
	arg2id(Arg2,Id2),
	arg2id(Arg3,Id3),
	( Id1 \== Id2 ),	% To avoid relations between same tokens
	relation( relation{ type => Type, arg1 => Id1, arg2 => Id2, arg3 => Id3 } )
	.

:-std_prolog arg2id/2.

arg2id(Arg,Id) :-
	( Arg = [] -> Id = []
	;   Arg = node{ cluster => C::cluster{ id => CId , left => Left , right => Right}} ->
	    ( recorded( redirect(CId,_CId) ) ->
		_Left is Left - 1
	    ;	
		_Left = Left,
		_CId = CId
	    ), 
	    (	f{ cid => _CId, id => Id } 
	    xor	cluster_overlap(C,C2::cluster{ id => CId2 }),
		f{ cid => CId2, id => Id }
	    )
	;   Id = Arg
	)
	.

:-light_tabular relation/1.

relation(relation{ id => Label }) :- rel_gensym(Label).

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => Cat1, cluster => cluster{ left => Left1 }},
	  arg2 => N2::node{ cluster => cluster{ right => Right2 }}
	} :-
	edge{ source => N3, target => N1, label => subject },
	(   recorded( terminal_v(N3,N2) )
	xor Cat1 = cln,
	    N2 = N3,
	    Left1 = Right2
	)
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	edge{ source => N3, target => N1, label => subject },
	edge{ source => N3, target => N4, label => label['S',vmod], type => adj },
	node!empty(N4),
	edge{ source => N4, target => N5::node{ cat => v }, type => subst },
	(   recorded( terminal_v(N5,N2) ) xor N2 = N5 )
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => Cat1, cluster => cluster{ left => Left1 }},
	  arg2 => N2::node{ cluster => cluster{ right => Right2 }}
	} :-
	edge{ source => N2, target => N1, label => subject },
	\+ edge{ source => N2, target => node{ cat => aux }, label => 'Infl', type => adj }
	.



relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	edge{ source => N3, target => N2, label => xcomp },
	verbose('Trying ctrl verb: ~E ~E\n',[N3,N2]),
	(   edge{ source => N3, target => N1, label => object }
	xor edge{ source => N3, target => N1, label => preparg }
	xor edge{ source => N3, target => N1, label => subject }
	)
	.

relation{ type => 'SUJ-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v }
	} :-
	E::edge{ source => N2,
		 target => N3::node{ cat => 'S', tree => Tree },
		 type => adj },
	domain('person_on_s',Tree),
	edge{ source => N3, target => N1, type => subst, label => 'N2' }
	.

relation{ type => 'AUX-V',
	  arg1 => N1::node{ cat => aux},
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1, label => 'Infl' }
	.

relation{ type => 'COD-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1,
	      label => label[object,ncpred,clg],
	      type => edge_kind[subst,lexical]
	    }
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N1, label => label[scomp,xcomp] },
	\+ edge{ source => N2, label => object }
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v},
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N3, label => 'preparg' },
	edge{ source => N3, target => N1, label => 'S' },
	\+ edge{ source => N2, label => object }
	.

relation{ type => 'COD-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N3::node{ cat => prep, lemma => de}, label => preparg },
	edge{ source => N3, target => N1 },
	\+ edge{ source => N2, label => object }
	.


relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v},
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N1, target => N2, label => 'V' },
	\+ edge{ source => N2, label => object }
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3, lemma => Lemma3 },
	      label => label[preparg]
	    },
	( Cat3 = prep ->
	    (	Lemma3 \== de 	% not true for all de-obj verb
	    xor edge{ source => N2, label => object } ),
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;   
	    N3 = N1
	)
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat },
	      label => label['S',vmod],
	      type => adj
	    },
	( node!empty(N3) ->
	    edge{ source => N3, target => N4::node{ cat => prep } }
	;   Cat = prep,
	    N3 = N4
	),
	(   edge{ source => N4, target => N1, type => edge_kind[subst,lexical] }
	;
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] },
	    N1 = node{ cat => prel }
	),
	\+ node!empty(N1)
	.


relation{ type => 'CPL-V',	% participiales
	  arg1 => N1::node{ cat => cat[v] },
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{},
	      label => label['S',vmod],
	      type => adj
	    },
	node!empty(N3),
	edge{ source => N3, target => N1, type => subst }
	.


relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1::node{ cat => cll }, label => cll }
	.


relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','vmod'],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N4::node{ cat => prep},
		  label => label['PP',wh], type => subst
		},
	E3::edge{ source => N4,
		  target => N1,
		  type => edge_kind[subst,lexical]
		},
	verbose('CPL-V ~E ~E ~E\n',[E1,E2,E3]),
	true
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => cat[adv,advneg,nc] },
	  arg2 => N2
	} :-
	edge{ source => N2,
	      target => N1,
	      label => label['V','V1','S','v','S2','vmod',advneg], type => edge_kind[adj,lexical] }
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => csu },
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N2,
	      target => N1,
	      label => advneg,
	      type => lexical
	    }
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => cat[pri,prel] },
	  arg2 => N2::node{ cat => v }
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','vmod'],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N1,
%%		  label => pri,
		  type => edge_kind[subst,lexical]
		},
	\+ edge{ source => N3,
		 target => node{ cat => prep }
	       }
	.



relation{ type => 'MOD-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => cat[csu] },
	      label => label['S','vmod'],
	      type => adj },
	edge{ source => N3, target => N1, label => label['SubS'], type => subst }
	.

relation{ type => 'COMP',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N3::node{ cat => cat[v]},
	      target => N2,
	      label => label[scomp,xcomp],
	      type => subst
	    },
	edge{ source => N3, target => N1, label => label[csu], type => lexical }
	.

%% COMP pour csu sans que: quand il vient, il mange

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => csu },
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N1,
	      target => N2,
	      label => label['SubS'],
	      type => subst
	    }
	.

%% COMP entre Prep et (GN GA ou NV) quand discontinus

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => prep },
	  arg2 => N2::node{ cat => cat[nc,cln,pro,np,pri,prel,v,adj] } } :-
	edge{ source => N1, target => N2, type => subst },
	'$answers'( group(const['GN','NV','GA'],_,_,N2,_) ),
	\+ '$answers'( group(const['GP','PV'],_,_,N1,_) )
	.

relation{ type => 'ATB-SO',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v},
	  arg3 => SO
	} :-
	edge{ source => N2, target => N1, label => comp },
	( edge{ source => N2, label => object } -> SO = objet ; SO = sujet )
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => nominal[] },
	  arg3 => faux
	} :-
	edge{ source => N2, target => N3::node{ cat => Cat3::cat[~coo] },
	      type => edge_kind[adj,lexical]
	    },
	( node!empty(N3) ->
	    %% Relatives, csu, participiales, ... 
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;   
	    N3 = N1
	)
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[adj,adv] },
	  arg2 => N2::node{ cat => ncpred },
	  arg3 => faux
	} :-
	edge{ source => N3::node{ cat => v},
	      target => N1,
	      type => adj,
	      label => ncpred
	    },
	edge{ source => N3,
	      target => N2,
	      type => lexical,
	      label => ncpred
	    }
	.


relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[det,number], lemma => '_NUMBER' },
	  arg2 => N2::node{ cat => nominal[] },
	  arg3 => faux
	} :-
	edge{ source => N2,
	      target => N1,
	      type => subst,
	      label => det
	    }
	.

relation{ type => 'MOD-N',
	  arg1 => N1,
	  arg2 => N1,
	  arg3 => vrai
	} :-
	N1::node{ cat => np, cluster => cluster{ left => Left, right => Right} },
	Left + 1 < Right
	.

relation{ type => 'MOD-A',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adj] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj
	    },
	( node!empty(N3) ->
	    %% not sure it may arise for adjectives !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

relation{ type => 'MOD-R',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adv,advneg] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj
	    },
%%	format('HERE ~w ~w\n',[N2,N3]),
	( node!empty(N3) ->
	    %% not sure it may arise for adverbs !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    %% not sure it may arise for adverbs !
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

relation{ type => 'MOD-P',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[prep] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj },
%%	format('HERE ~w ~w\n',[N2,N3]),
	( node!empty(N3) ->
	    %% not sure it may arise for prepositions !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    %% not sure it may arise for prepositions !
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => N2::node{},
	  arg3 => N3::node{}
	} :-
	edge{ source => Start::node{},
	      target => LastCoord::node{ cat => cat[coo] },
	      type => adj
	    },
	(   _N2 = Start
	;   edge{ source => LastCoord, target => _N2, label => coord2 }
	),
	coord_next(LastCoord,_N2,Coord),
	coord_next(LastCoord,Coord,_N3),
	( node!empty(_N2) ->
	    edge{ source => _N2,
		  target => N2::node{ cat => cat[v,prep,nc,np] },
		  type => edge_kind[subst,lexical]
		}
	;   
	    N2 = _N2
	),
	( node!empty(_N3) ->
	    edge{ source => _N3,
		  target => N3::node{ cat => cat[v,prep,nc,np] },
		  type => edge_kind[subst,lexical]
		}
	;   
	    N3 = _N3
	)

	.


relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => N2::node{},
	  arg3 => N3::node{}
	} :-
	edge{ source => Start::node{},
	      target => Enum::node{ tree => Tree },
	      type => adj
	    },
	domain('N2_enum',Tree),
	LastCoord = Enum,
	(   _N2 = Start
	;   edge{ source => LastCoord, target => _N2, label => coord2 }
	),
	coord_next(LastCoord,_N2,Coord),
	coord_next(LastCoord,Coord,_N3),
	( node!empty(_N2) ->
	    edge{ source => _N2,
		  target => N2::node{ cat => cat[v,prep,nc,np] },
		  type => edge_kind[subst,lexical]
		}
	;   
	    N2 = _N2
	),
	( node!empty(_N3) ->
	    edge{ source => _N3,
		  target => N3::node{ cat => cat[v,prep,nc,np] },
		  type => edge_kind[subst,lexical]
		}
	;   
	    N3 = _N3
	)

	.


:-std_prolog coord_next/3.

coord_next( LastCoord::node{},
	    Start::node{ cluster => cluster{ right => StartRight }},
	    Next::node{ cluster => cluster{ left => NextLeft }}
	  ) :-
%%	format('Search coord next: ~w\n',[Start]),
	(   Next = LastCoord
	;   edge{ source => LastCoord,
		  target => Next,
		  label => label[void,coord2,coord3]
		}
	),
	NextLeft >= StartRight,
	\+ ( ( edge{ source => LastCoord,
		     target => Next2::node{ cluster => cluster{ left => NextLeft2 }},
		     label => label[void,coord2,coord3] }
	     ;	 Next2 = LastCoord
	     ),
	       NextLeft2 >= StartRight,
	       NextLeft2 < NextLeft
	   ),
%%	format('\n\t => ~w\n',[Next]),
	true
	. 


relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N2, target => N3::node{ cat => 'N2' }, type => adj },
%%	format('HERE ~w ~w\n',[N2,N3]),
	node!empty(N3),
	edge{ source => N3, target => N1, type => subst, label => 'N2app' }
	.

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N2, target => N1::node{ cat => np }, label => 'np' }
	.


%% JUXT: to be done

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Decoding relations

:-extensional xrelation/2.

xrelation( relation{ type => Type::'SUJ-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(sujet,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'AUX-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(auxiliaire,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COD-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(cod,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'CPL-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(complement,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COMP', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(complementeur,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'ATB-SO', arg1 => Id1, arg2 => Id2, arg3 => SO, id => Id },
	   xrelation(Type,Id,[ arg(attribut,Id1), arg(verbe,Id2), so(SO) ] )
	 ).

xrelation( relation{ type => Type::'MOD-N', arg1 => Id1, arg2 => Id2, arg3 => Prop, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arg(nom,Id2), '�-propager'(Prop) ] )
	 ).

xrelation( relation{ type => Type::'MOD-A', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arg(adjectif,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-R', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arg(adverbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-P', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arg(preposition,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COORD', arg1 => Id1, arg2 => Id2, arg3 => Id3, id => Id },
	   xrelation(Type,Id,[ arg(coordonnant,Id1),arg('coord-g',Id2), arg('coord-d',Id3) ] )
	 ).

xrelation( relation{ type => Type::'APPOS', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(premier,Id1), arg(appose,Id2) ] )
	 ).

xrelation( relation{ type => Type::'JUXT', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(premier,Id1), arg(suivant,Id2) ] )
	 ).

:-rec_prolog f2groups/2.

f2groups([],[]).
f2groups([Arg|Args],[XArg|XArgs]) :-
	f2groups(Args,XArgs),
	(   Arg = arg(Type,FId),
	    f2group(FId,GId) ->
	    \+ domain(arg(_,GId),XArgs),
	    XArg = arg(Type,GId)
	;   
	    XArg = Arg
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing XML objects
%%   extended for EASy Objects

%% event_super_handler(default(Stream),default(Stream)).

event_process( H::default(Stream), xmldecl, Ctx, Ctx ) :-
	format(Stream,'<?~w~L?>',[xml,[' ~A',' '],[version:'1.0',encoding:'ISO-8859-1']])
	.
	
event_process( H::default(_), C::cluster{ id => Id, left => Left },Ctx1,Ctx2 ) :-
	event_process(H,iterate_in_cluster(Id,Left),Ctx1,Ctx2).

/*
event_process( H::default(_), iterate_in_cluster(Id,Left),Ctx1,Ctx3 ) :-
	( f{ cid => Id, left => Left, right => Right } ->
	    mutable(M,[]),
	    every(( F::f{ cid => _Id, left => Left, right => Right },
		    mutable_read(M,_L),
		    f_sorted_add(F,_L,_LL),
		    mutable(M,_LL)
		  )),
	    mutable_read(M,L),
	    event_process( H, fs(L), Ctx1,Ctx2 ),
	    event_process( H, iterate_in_cluster(Id,Right), Ctx2, Ctx3)
	;
	    Ctx1=Ctx3
	)
	.
*/

event_process( H::default(_), iterate_in_cluster(CId,Left),Ctx1,Ctx2 ) :-
	mutable(M,[]),
	every(( F::f{ cid => CId },
		mutable_read(M,_L),
		f_sorted_add(F,_L,_LL),
		mutable(M,_LL)
	      )),
	mutable_read(M,L),
%%	format('Emit Cluster: ~w => ~w\n',[CId,L]),
	event_process( H, fs(L), Ctx1,Ctx2 )
	.

:-rec_prolog f_sorted_add/3.

f_sorted_add(F,[],[F]).
f_sorted_add(F1::f{ id => Id1, left => Left1, right=> Right1 }, L::[F2::f{ id =>  Id2 , left => Left2, right => Right2}|L2],XL) :-
	(  Left1 =< Left2, Right1 =< Right2, Id1 @< Id2 ->
	    XL = [F1|L]
	;
	    XL = [F2|XL2],
	    f_sorted_add(F1,L2,XL2)
	)
	.
	    

event_process( H::default(_),fs([]),Ctx,Ctx).
event_process( H::default(_),fs([F|L]),Ctx1,Ctx3) :-
	event_process(H,F,Ctx1,Ctx2),
	event_process(H,fs(L),Ctx2,Ctx3)
	.

event_process( H::default(_),f{ id => Id, lex => Lex },Ctx1,Ctx4) :-
	(   recorded( emitted(Id) ),
	    Ctx4 = Ctx1	    
	xor event_process( H, start_element{ name => 'F', attributes => [id:Id] }, Ctx1, Ctx2 ),
	    event_process( H, characters{ value => Lex }, Ctx2, Ctx3 ),
	    event_process( H, end_element{ name => 'F' }, Ctx3, Ctx4 ),
	    record_without_doublon( emitted(Id) )
	)
	.

:-extensional f2group/2.

event_process( H::default(_),group(Type,_,_,_,Content),Ctx1,Ctx4) :-
	group_gensym(Id),
	every(( domain(CId,Content),
		f{ cid => CId, id => FId },
		record_without_doublon( OO::f2group(FId,Id) ))),
	event_process( H, start_element{ name => 'Groupe', attributes => [id:Id,type:Type] }, Ctx1, Ctx2 ),
	event_process( H, flist(Content), Ctx2, Ctx3 ),
	event_process( H, end_element{ name => 'Groupe' }, Ctx3, Ctx4 )
	.

event_process( H::default(_), flist([]), Ctx, Ctx ).
event_process( H::default(_), flist([Id|L]), Ctx1, Ctx3 ) :-
	C::cluster{id => Id },
	event_process(H,C,Ctx1,Ctx2),
	event_process(H,flist(L),Ctx2,Ctx3)
	.

event_process( H::default(_), constituants, Ctx1, Ctx3) :-
	event_process( H, start_element{ name => 'constituants', attributes => [] }, Ctx1, Ctx2 ),
	event_process( H, iterate_constituant(0), Ctx2, Ctx3 )
	.

event_process( H::default(_), iterate_constituant(N), Ctx1, Ctx3 ) :-
%%	format('ITERATE CONSTITUANTS ~w',[N]),
	(   constituant(Left,Right,Const),
	    Left >= N,
	    \+ ( constituant(_Left,_,_), _Left >= N, Left > _Left )
	->
%%	    format('CONST ~w\n',[Const]),
	    event_process(H,Const,Ctx1,Ctx2),
	    event_process(H,iterate_constituant(Right),Ctx2,Ctx3)
	;   
	    event_process(H,end_element{ name => 'constituants' }, Ctx1,Ctx3)
	)
	.

event_process( H::default(_), relations, Ctx1, Ctx4 ) :-
	event_process(H,start_element{ name => relations, attributes => []}, Ctx1, Ctx2 ),
	mutable(Ctx,Ctx2),
	every(( domain(Type,rel[]),
		'$answers'( relation(R::relation{ type => Type }) ),
		xrelation(R,XR::xrelation(Type,Id,Content)),
		(   f2groups(Content,Content1)		   
		xor Content = Content1 ),
		event_process(H,xrelation(Type,Id,Content1),Ctx)
	      )),
	mutable_read(Ctx,Ctx3),
	event_process(H,end_element{ name => relations }, Ctx3, Ctx4 )
	.

event_process( H::default(_), xrelation(Type,Label,Content), Ctx1, Ctx4) :-
	event_process(H,
		      start_element{ name => relation,
				     attributes => [ xlink!type: extended,
						     type: Type,
						     id: Label ] },
		      Ctx1,
		      Ctx2 ),
	mutable(Ctx,Ctx2),
	event_process(relarg(H),Content,Ctx),
	mutable_read(Ctx,Ctx3),
	event_process(H, end_element{ name => relation }, Ctx3, Ctx4 )
	.

event_process( relarg(H), arg(Type,Label), Ctx1, Ctx2 ) :-
	event_process(H,
		      element{ name => Type,
			       attributes => [ xlink!type: locator,
					       xlink!href: Label
					     ]
			     },
		      Ctx1, Ctx2
		     )
	.

event_process( relarg(H), so(SO), Ctx1, Ctx2 ) :-
	event_process(H,
		      element{ name => 's-o',
			       attributes => [valeur: SO]
			     },
		      Ctx1, Ctx2
		     ).

event_process( relarg(H), '�-propager'(Prop), Ctx1, Ctx2 ) :-
	(Prop = faux xor true),
	event_process(H,
		      element{ name => '�-propager',
			       attributes=> [booleen: Prop]
			     },
		      Ctx1, Ctx2
		     )
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Handler

event_handler( Reader,
	       OE::start_element{ name => E::dependencies,
				  attributes => Attrs },
	       OE,
	       [],
	       []
	     ) :-
	attrs([mode:Mode], Attrs),
	(   Mode = 'full' xor true ),
	record( mode(Mode) )
	.

event_handler( Reader,
	       OE::start_element{ name => cluster,
				  attributes => Attrs,
				  empty => true
				},
	       OE,
	       [],
	       []
	     ) :-
	attrs([id:Id,left:XLeft,right:XRight,lex:Lex], Attrs),
	utf8_to_xmllatin1(Lex,Latin_Lex),
	atom_number(XLeft,Left),
	atom_number(XRight,Right),
	/*
	label2lex(Latin_Lex,_,FIds),
	format('label2lex ~w => ~w\n',[Latin_Lex,FIds]),
	(   FIds = [] ->
	    TLeft = Left,
	    TRight = Right
	;
	    FIds = [F1|_],
	    format('try conversion ~w\n',[F1]),
	    atom_number(F1,TLeft),
	    format('=> TLeft = ~w\n',[TLeft]),
	    length(FIds,N),
	    TRight is TLeft+N,
	    format('=> TRight = ~w\n',[TRight]),
	    true
	),
        */
	Cluster = cluster{ id => Id,
			   left => Left,
			   right => Right,
			   lex => Latin_Lex
			 },
	( Right > Left + 1, Latin_Lex == '_' ->
	    %% bug in frmg => temporary special handling
	    record( tmp(Cluster) )
	;   
	    record( Cluster )
	),
%%	format('~w lex=~w latin=~w\n',[Cluster,Lex,Latin_Lex]),
	true
	.



:-light_tabular fix_clusters/0.

fix_clusters :-
	%% post readin correction on some clusters
	%% because of bug in frmg
	every(( recorded( tmp(cluster{ id => Id,
				       left => Left,
				       right => Right
				     }) ),
		glue_clusters(Left,Right,Lex),
		verbose('clue cluster ~w ~w => ~w\n',[Left,Right,Lex]),
		record( cluster{ id => Id,
				 left => Left,
				 right => Right,
				 lex => Lex
			       } )))
	.

:-std_prolog glue_clusters/3.

glue_clusters(Left,Right,Lex) :-
	( Left == Right ->
	    Lex = ''
	;   
	    recorded( cluster{ left => Left,
			       right => Middle,
			       lex => Lex1 } ),
	    Lex1 \== '',
	    Middle > Left,
	    glue_clusters(Middle,Right,Lex2),
	    ( Lex2 == '' ->
		Lex = Lex1
	    ;	
		name_builder('~w ~w',[Lex1,Lex2],Lex)
	    )
	)
	.

event_handler( Reader,
	       OE::start_element{ name => node,
				  attributes => Attrs,
				  empty => true
				},
	       OE,
	       [],
	       []
	     ) :-
	fix_clusters,
	attrs([id:Id, cluster:CId, (tree):Tree, cat:Cat, lemma: Lemma, xcat: XCat, deriv: Derivs], Attrs),
	recorded(Cluster::cluster{ id => CId }),
	rx!tokenize(Tree,' ',XTree),
	(Derivs = [] xor true),
	rx!tokenize(Derivs,' ',XDerivs),
	Node = node{ id => Id,
		     cluster => Cluster,
		     tree => XTree,
		     cat => Cat,
		     lemma => Lemma,
		     xcat => XCat,
		     deriv => XDerivs
		   },
	record( Node ),
%%	format('~w\n',[Node]),
	true
	.

event_handler( Reader,
	       start_element{ name => E::edge,
			      attributes => Attrs
				},
	       OE::end_element{ name => E },
	       [],
	       []
	     ) :-
	attrs([id:Id,source:SourceId,target:TargetId,type:Type,label:Label], Attrs),
	recorded( Source::node{ id => SourceId } ),
	recorded( Target::node{ id => TargetId } ),
	Edge = edge{ id => Id,
		     source => Source,
		     target => Target,
		     type => Type,
		     label => Label,
		     deriv => Deriv
		   },
	event_handler( Reader, loop, OE, Edge * [], Edge * Deriv),
%%	format('~w\n',[Edge]),
	record( Edge ),
	true
	.

event_handler( Reader,
	       OE::start_element{ name => deriv,
				  attributes => Attrs,
				  empty => true
				},
	       OE,
	       (Edge :: edge{ id => EId }) * Deriv,
	       Edge * [Name|Deriv]
	     ) :-
	attrs([name:Name,span:Span],Attrs),
	record_without_doublon(deriv(Name,EId,Span))
	.

:-rec_prolog attrs/2.

attrs([],_).
attrs([A:V|Attrs],All_Attrs) :-
	(   domain(A:V,All_Attrs) xor true ),
	attrs(Attrs,All_Attrs)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Tools

:-std_prolog utf8_to_xmllatin1/2.

utf8_to_xmllatin1(UTF8,Latin1) :-
	%%	format('Call with ~w\n',[UTF8]),
	%% Conversion to latin1 + replacement of &<> by entities
	'$interface'( 'UTF8_to_XMLLatin1'(UTF8:string), [ return(Latin1:string) ] )
	.

:-std_prolog name_builder/3.

name_builder(Format,Args,Name) :-
        string_stream(_,S),
        format(S,Format,Args),
        flush_string_stream(S,Name),
        close(S)
        .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Primitives to navigates within dependencies


:-light_tabular node!neighbour/3.
:-mode(node!neighbour/3,+(+,+,-)).

node!neighbour(left, X, N::node{} ) :-
	(   X = node{ cluster => ClusterA::cluster{ left => P} } xor X = P ),
	ClusterB::cluster{ right => P },
	N::node{ cluster => ClusterB }
	.

node!neighbour(right, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ right => P} } xor X = P ),
	ClusterB::cluster{ left => P },
	N::node{ cluster => ClusterB }
	.

node!neighbour(xleft, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ left => P} } xor X = P ),
	ClusterB::cluster{ right => Q }, Q =< P,
	N::node{ cluster => ClusterB }
	.

node!neighbour(xright, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ right => P } } xor X = P ),
	ClusterB::cluster{ left => Q }, P =< Q,
	N::node{ cluster => ClusterB }
	.

:-std_prolog node!empty/1.

node!empty( node{ cluster => cluster{ lex => ''} } ).


:-light_tabular node!dependency/4.
:-mode(node!dependency/4,+(+,+,-,-)).

node!dependency(out,N1::node{},N2::node{},[Edge]) :-
	Edge::edge{ source => N1, target => N2 }
	.

node!dependency(in,N1::node{},N2::node{},[Edge]) :-
	Edge::edge{ target => N1, source => N2 }
	.

/*
node!dependency(xout,N1::node{},N2::node{},L) :-
	@*{ goal => ( _E::edge{ source => _N1, target => _N2 },
			\+ domain(edge{ source => _N2 },_L)
		    ),
	    from => 1,
	    collect_first => [N1,[]],
	    collect_last  => [N2,L],
	    collect_loop => [ _N1::node{}, _L],
	    collect_next => [ _N2::node{}, [_E|_L] ]
	  }
	.
*/

node!dependency(xout,N1::node{},N2::node{},L) :-
	node!dependency(out,N1,N3,[E]),
	N1 \== N3,
	(   N2 = N3,
	    L = [E]
	;   
	    node!dependency(xout,N3,N2,L1),
	    (\+ domain( edge{ target => N1 }, L1 )),
	    L = [E|L1]
	)
	.

node!dependency(xin,N1::node{},N2::node{},L) :-
	@*{ goal => ( _E::edge{ target => _N1, source => _N2 },
			\+ domain( edge{ target => _N2 },L),
			_N2 \== N1 % to avoid loop
		    ),
	    from => 1,
	    collect_first => [N1,[]],
	    collect_last  => [N2,L],
	    collect_loop => [ _N1::node{}, _L],
	    collect_next => [ _N2::node{}, [_E|_L] ]
	  }
	.

:-light_tabular node!safe_dependency/3.
:-mode(node!safe_dependency/4,+(+,+,-)).

node!safe_dependency(xout,N1::node{},N2::node{}) :-
	node!dependency(out,N1,N3,_),
	(   N2 = N3
	;   
	    node!safe_dependency(xout,N3,N2)
	),
	N1 \== N2
	.

node!safe_dependency(xin,N1::node{},N2::node{}) :-
	node!dependency(in,N1,N3,_),
	(   N2 = N3
	;   
	    node!safe_dependency(xin,N3,N2)
	),
	N1 \== N2
	.


:-xcompiler(( node!collect(X^G,L) :-
	    mutable(M,[]),
	      every((G,
		     mutable_read(M,L1),
		     (	 \+ domain(X,L1)),
		     X = node{ cluster => cluster{ lex => LexX}},
		     LexX \== '',		     
		     node!add(X,L1,L2),
		     mutable(M,L2))),
	      mutable_read(M,L))).


:-rec_prolog node!add/3.

node!add( N::node{},[],[N]).
node!add( N::node{ cluster => Cluster::cluster{ left => Left}},
	  L1::[N1::node{ cluster => Cluster1::cluster{ left => Left1}}|XL1],
	  L2
	) :-
	( Cluster == Cluster1 ->
	    L2 = L1
	;   
	    Left < Left1 ->
	    L2 = [N|L1]
	;   
	    node!add(N,XL1,XL2),
	    L2 = [N1|XL2]
	),
%%	format('NODE ADD ~w\n',[L2]),
	true
	.

:-rec_prolog node!add_fillers/2.

node!add_fillers( L::[N::node{}], L ).
node!add_fillers( L::[N1::node{ cluster => cluster{ left => Left1, right => Right1 }},
		      N2::node{ cluster => cluster{ left => Left2, right => Right2 }} | LL ],
		  XL::[N1|XL2]
		 ) :-
	(   N::node{ cluster => cluster{ lex => _Lex, left => Right1, right => _Left } },
	    _Lex \== '',
	    _Left =< Left2 ->
	    node!add_fillers([N,N2|LL],XL2)
	;
	    node!add_fillers([N2|LL],XL2)
	)
	.

:-std_prolog node!terminal/3.

node!terminal(N,Cat,Path) :-
	\+ ( node!dependency(out,N, node{ cat => Cat },[edge{ label => Label} ]),
	       domain(Label,Path)
	   )
	.

:-std_prolog node!parent/4.

node!parent(N,Cat,P,Path) :-
	node!dependency(in,N,P::node{ cat => XCat },[edge{ label => Label }]),
	domain(Label,Path),
	domain(XCat,Cat)
	.

:-std_prolog node!older_ancestor/4.

node!older_ancestor(N,Cat,P,Path) :-
	(   node!parent(N,Cat,P1,Path),
	    node!older_ancestor(P1,Cat,P,Path)
	xor P = N
	).

:-std_prolog node!first_v_ancestor/2.

node!first_v_ancestor(N::node{cat => Cat::cat[aux,v], tree => Tree}, N1) :-
	( Cat == v ->
	    N1 = N
	;   Cat == aux, domain(cleft_verb,Tree) ->
	    N1 = N
	;   
	    node!parent(N,cat[aux,v],N2,['Infl','V']),
	    node!first_v_ancestor(N2,N1)
	)
	.

%% BUG SOMEWHERE IN DYALOG on @*

:-light_tabular node!group/4.
:-mode(node!group/4,+(+,-,-,-)).

node!group(Nodes,Words,Left,Right) :-
	%%	format('Start ~w\n',[Nodes]),
	@*{ goal => ( _Nodes1 = [ N::node{ cluster => cluster{ id => Id,
							       left => CLeft,
							       right => CRight }}
				| _Nodes],
			( domain(Id,Words) ->
			    _Words1 = _Words
			;   
			    _Words1 = [Id|_Words]
			),
%%			format('HERE ~w ~w ~w\n',[_Nodes,_Left,_Right]),
			( CLeft < _Left -> NLeft = CLeft ; NLeft = _Left ),
			( CRight > _Right -> NRight = CRight ; NRight = _Right ),
			true
		    ),
	    %%	    from => 0,
	    %%	    vars => [Words],
	    collect_first => [Nodes,Words,1000,0],
	    collect_last => [[],[],Left,Right],
	    collect_loop => [_Nodes1,_Words1,_Left,_Right],
	    collect_next => [_Nodes,_Words,NLeft,NRight]
	  },
	%% format('End ~w ~w ~w\n',[Words,Left,Right]),
	true
	.


:-light_tabular label2lex/3.
:-mode(label2lex/3,+(+,-,-)).

label2lex(Label,Lex,FIds) :-
	rx!tokenize(Label,' ',Labels),
	label2lex_aux(Labels,Lex,FIds).

:-std_prolog label2lex_aux/3.

label2lex_aux(Labels,Lex,FIds) :-
	(   Labels = [] -> Lex=[], FIds = []
	;   Labels = [X|Labels2],
	    (	rx!tokenize(X,'|',[FX,LX]) xor LX=X, FX='' ),
	    Lex = [LX|Lex2],
%%	    format('Try match ~w\n',[FX]),
	    rx!match{ string => FX,
		      rx => rx{ pattern => '\\([0-9][0-9]*\\)$' },
		      substrings => [_FX],
		      subs_flag => 1
		    },
%%	    format('=> ~w\n',[_FX]),
	    FIds = [_FX|FIds2],
	    label2lex_aux(Labels2,Lex2,FIds2)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%:-std_prolog verbose/2.

:-xcompiler(( verbose(Msg,Args) :- (	\+ opt(verbose) xor format(Msg,Args)))).
%%:-xcompiler((verbose(Msg,Args) :- true )).


:-std_prolog reverse/3.

reverse(L,LL,Acc) :-
        ( L=[A|B] -> reverse(B,[A|LL],Acc) ; Acc=LL)
        .

:-std_prolog record_without_doublon/1.

record_without_doublon( A ) :-
        (recorded( A ) xor record( A ))
        .

:-std_prolog update_counter/2.

update_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_inc(M,V)
        ;   V=1,
            mutable(M,2),
            record( counter(Name,M) )
        )
        .

:-std_prolog value_counter/2.

value_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_read(M,V)
        ;
            V = 1
        )
        .

:-std_prolog group_gensym/1.

group_gensym(L) :-
	sentence(Sent),
	update_counter(group,X),
	name_builder('E~wG~w',[Sent,X],L)
	.

:-std_prolog rel_gensym/1.

rel_gensym(L) :-
	sentence(Sent),
	update_counter(relation,X),
	name_builder('E~wR~w',[Sent,X],L)
	.

:-std_prolog mark_as_used/1.

mark_as_used(L) :-
	every(( domain( Label, L), record_without_doublon(used(Label)))).


format_hook(0'E,Stream,[edge{ id => Id, source => N1, target => N2, label => L}|R],R) :-
        format(Stream,'~w:~E-~w->~E',[Id,N1,L,N2])
        .

format_hook(0'E,Stream,[node{ id => Id,cat => Cat,cluster => C }|R],R) :-
        format(Stream,'~w/~w/~E',[Id,Cat,C])
        .

format_hook(0'E,Stream,[cluster{ id => Id, lex => Lex }|R],R) :-
	format(Stream,'~w{~w}',[Id,Lex])
	.

format_hook(0'e,Stream,[Id|R],R) :-
	E::edge{ id => Id },
	format(Stream,'~E',[E])
	.

format_hook(0'L,Stream,[[Format,Sep],AA|R],R) :-
        mutable(M,0,true),
        every((   domain(A,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A]) ))
        .


:-std_prolog random/1.

random(X) :- '$interface'( random, [return(X:int)] ).


			 
