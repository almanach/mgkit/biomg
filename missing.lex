## My completion file for adding missing features to words
## beware of tabs as separator (not blanks)

## souvent	adv	[adv_kind=freq]

@adj sigmo�de +s + +s
@adj ombelliforme +s + +s
@adj appr�me +s + +s
@adj sacciforme +s + +s
@adj calc�iforme +s + +s
@adj lenticell� +s +e +es
@adj lin�aire-lanc�ol� lin�aires-lanc�ol�s lin�aire-lanc�ol�e lin�aires-lanc�ol�es
@adj cochl�iforme +s + +s
@adj subobtus + +e +es
@adj subdress� +s +e +es
@adj reniforme +s + +s
@adj punctiforme +s + +s
@adj subcord� +s +e +es
@adj spatule +s + +s
@adj claviforme +s + +s
@adj suturai +s +e +es
@adj subcirculaire +s + +s
@adj piriforme +s + +s
@adj imparipenn� +s +e +es
@adj rhombiforme +s + +s
@adj subellipso�de +s + +s
@adj pal�otropical pal�otropicaux pal�otropicale pal�otropicales
@adj infundibuliforme + + +s
@adj p�talo�de +s + +s
@adj ovoluf�re +s + +s
@adj tubuleux tubuleux tubuleuse tubuleuses
@adj pub�rulent +s +e +es
@adj laxiflore +s + +s
@adj flexueux flexueux flexueuse flexueuses
@adj staminodial staminodiaux staminodiale staminodiales
@adj bract�ol� +s +e +es
@adj aran�eux aran�eux aran�euse aran�euses
@adj cymeux cymeux cymeuse cymeuses
@adj rac�meux rac�meux rac�meuse rac�meuses
@adj ov� +s +e +es
@adj r�supin� +s +e +es
@adj condupliqu� +s +e +es
@adj fov�ol� +s +e +es
@adj panicul� +s +e +es
@adj opercul� +s +e +es
@adj apicul� +s +e +es
@adj arist� +s +e +es 
@adj flabell� +s +e +es
@adj collicul� +s +e +es
@adj incombent +s +e +es
@adj pelliculeux pelliculeux pelliculeuse pelliculeuses
@adj ongul� +s +e +es
@adj marcescent +s +e +es
@adj s�tuleux s�tuleux s�tuleuse s�tuleuses
@adj squameux squameux squameuse squameuses
@adj li�geux li�geux li�geuse li�geuses
@adj verticill� +s +e +es
@adj mucilagineux mucilagineux mucilagineuse mucilagineuses
@adj sulqu� +s +e +es
@adj fimbri� +s +e +es
@adj r�sinif�re +s + +s
@adj sectile +s + +s
@adj saprophyte +s + +s
@adj dimidi� +s +e +es
@adj cr�nul� +s +e +es
@adj �chinul� +s +e +es
@adj mucronul� +s +e +es
@adj rud�ral rud�raux rud�rale rud�rales
@adj anatrope +s + +s
@adj chartac� +s +e +es
@adj vicariant +s +e +es
@adj velutineux velutineux velutineuse velutineuses
@adj papilleux papilleux papilleuse papilleuses
@adj s�tac� +s +e +es
@adj ligule +s + +s
@adj glabrescent +s +e +es
@adj stramin� +s +e +es

@adj p�tiol� +s +e +es
@adj auricul� +s +e +es
@adj p�dicell� +s +e +es
@adj tridente +s + +s
@adj bidente +s + +s
@adj trident� +s +e +es
@adj p�doncul� +s +e +es
@adj opercul� +s +e +es
@adj bifoli� +s +e +es
@adj trifoli� +s +e +es
@adj p�dicell� +s +e +es

@adj scabre +s + +s
@adj jugu� +s +e +es
@adj exsert +s +e +es
@adj �pip�tale +s + +s
@adj alternip�tale +s + +s
@adj alternis�pale +s + +s

@adj triparti +s +e +es
@adj biparti +s +e +es

@adj monosperme +s + +s

## The following should be corrected in Lefff (invariant adj)
@adj marron + + + +

## The following is maybe �toil�
@adj �toile +s + +s

## @adj blanc-ros�

##basai	adj	[pred='basai<Suj:(sn),Objde:(de-scompl|de-sinf|de-sn),Obj�:(�-scompl|�-sinf|�-sn)>',cat=adj,@s]

## @nc stylopode +s @
@nc staminode +s @
@nc testa +s @
@nc m�ricarpe +s @m
@nc exocarpe +s @m
@nc convergente +s @f
@nc pistillode +s @
@nc costula costulae @m
@nc costa costae @m
@nc cymule +s @f
@nc hypocotyle +s @m
@nc phlo�me +s @m
@nc vall�cule +s @f
@nc bract�ole +s @f
@nc �picotyle +s @m
@nc pr�feuille +s @f
@nc xyl�me +s @m
@nc panicule +s @f
@nc ext�rieur +s @m
@nc endocarpe +s @m
@nc deux-tiers deux-tiers @m
@nc exsud�t +s @m
@nc rostelle +s @m
@nc pseudobulbe +s @m
@nc hypanthe +s @m
@nc p�rigone +s @m
@nc apicule +s @m
@nc pollinie +s @f
@nc st�llidie +s @
@nc acumen +s @m
@nc callus +s @m
@nc micropyle +s @m
@nc papille +s @f
@nc stipelle +s @f
@nc radicule +s @f
@nc involucelle +s @m
@nc gynophore +s @m
@nc cuculle +s @f
@nc domatie +s @f
@nc verticille +s @m
@nc monoth�que +s @f
@nc carpophore +s @m
@nc dessication +s @f
@nc saprophyte +s @m
@nc ongul� +s @m
@nc lenticelle +s @f
@nc gynost�me +s @m

@nc bract�ole +s @f
@nc p�tiolule +s @m
@nc panicule +s @f
@nc involucre +s @m
@nc stipule +s @m
@nc domatie +s @f
@nc acumen +s @m

# _DIM as np needed for 'mesure _DIMENSION'
## _DIMENSION	np	[cat=np,@p]
##_DIMENSION	nc	[pred='_DIMENSION_____1<Suj:(sn),Objde:(de-sn|de-sinf),Obj:(�-sinf)>',cat=nc,sat=+]	_DIMENSION	Default
_DIMENSION	adj	[pred='_DIMENSION_____3<Suj:(sn),Objde:(de-scompl|de-sinf|de-sn),Obj�:(�-scompl|�-sinf|�-sn)>',cat=adj]	_DIMENSION	Default

##dens�ment	adv	[]
##dessus	adv     []
##dessous	adv     []
##basalement adv []
##dorsalement adv []
$	adj	[pred='male<Suj:(sn),Objde:(de-scompl|de-sinf|de-sn),Obj�:(�-scompl|�-sinf|�-sn)>',cat=adj]
�	adj	[pred='femelle<Suj:(sn),Objde:(de-scompl|de-sinf|de-sn),Obj�:(�-scompl|�-sinf|�-sn)>',cat=adj]

<	prep	[pred='inf�rieur �<Obj:sn>',cat=prep]
>	prep	[pred='sup�rieur �<Obj:sn>',cat=prep]

##certain	det	[det=+,define=-,@ms]
##certains	det	[det=+,define=-,@mp]
##certaine	det	[det=+,define=-,@fs]
##certaines	det	[det=+,define=-,@mp]
face_�_face	adj	[pred='face_�_face',cat=adj]	face_�_face____1	default:

## radialement	adv []

######################################################################
## Added June 26th

## abaxialement 82
abaxialement	adv []
## abyssinie 66
##Abyssinie	np	[cat=np,@fs]
## aciculiformes 9
@adj aciculiforme +s + +s
## acrosticho�de 52
@adj acrosticho�de +s + +s
## afroam�ricaine 9
@adj afroam�ricain +s +e +es
## aig�ment 9
aig�ment	adv []
## amazonie 55
Amazonie	np	[cat=np,@fs]
## anth�rophores 544
@nc anth�rophore +s @m
## bengale 6
Bengale	np	[cat=np,@ms]
## bitubercul� 78
@adj bitubercul� +s +e +es
## bract�iformes 765
@adj bract�iforme +s + +s
## caud�e 8
@adj caud� +s +e +es
## caulinaire 9
@adj caulinaire +s + +s
## cayenne 5
Cayenne	np	[cat=np,@ms]
## ceylan 71
Ceylan	np	[cat=np,@ms]
## chasmogames 71
@adj chasmogame +s + +s
## circumscissile 69
@adj circumscissile +s + +s
## clinandre 51
@nc clinandre +s @m
## coalescents 72
@adj coalescent +s +e +es
## cocculoides 6
@adj cocculoide +s + +s
## consp�cifiques 54
@adj consp�cifique +s + +s
## convolut�es 6
@adj convolut� +s +e +es
## cotyl�donaire 53
@adj cotyl�donaire +s + +s
## crateriformes 92
@adj crateriforme +s + +s
## cynomelroides 6
@adj cynomelroide +s + +s
## dahomey 68
Dahomey	np	[cat=np,@ms]
## dentiformes 66
@adj dentiforme +s + +s
## disciformes 76
@adj disciforme +s + +s
## distalement 57
distalement	adv []
## dorsifixes 61
adj dorsifixe +s + +s
## entrenoeuds 63
@nc entrenoeud +s @m
## �parsement 74
�parsement	adv []
## exalbumin�e 98
@adj exalbumin� +s +e +es
## exondation 71
@nc exondation +s @
## flabelliforme 8
@adj flabelliforme +s + +s
## glucidique 95
@adj glucidique +s + +s
## hilob� 79
@adj hilob� +s +e +es
## holotype 95
@nc holotype +s @m
## ichthyotoxiques 9
@adj ichthyotoxique +s + +s
## indent� 6
@dj indent� +s +e +s
## in�quilat�rales 8
@adj in�quilat�rale +s + +s
## interloculaires 6
@adj interloculaire +s + +s
## intrap�tiolaires 7
@adj intrap�tiolaire +s + +s
## involutes 83
@adj involute +s + +s
## lacini� 62
@lacini� +s +e +es
## lacuneux 9
@adj lacuneux lacuneux lacuneuse lacuneuses
## lamelleux 61
@adj lamelleux lamelleux lamelleuse lamelleuses
## lanc�ol�s 94
@adj lanc�ol� +s +e +es
## loculaire 70
@adj loculaire +s + +s
## mississipi 6
Mississipi	np	[cat=np,@ms]
## monog�n�rique 7
@adj monog�n�rique +s + +s 
## monosp�cifique 53
@adj monosp�cifique +s + +s
## mucron�es 78
@adj mucron� +s +e +es
## multicellulaires 70
@adj multicellulaire +s + +s
## multiflores 88
@adj multiflore +s + +s
## multiovul�e 83
@adj multiovul� +s +e +es
## mycorrhizes 50
@nc mycorrhize +s @m
## n�otype 85
@nc n�otype +s @m
## nerv�s 52
@adj nerv� +s +e +es
## oblanc�ol�e 53
@adj oblanc�ol� +s +e +es
## oblongo�de 66
@adj oblongo�de +s + +s
## oeuvrable 5
@adj oeuvrable +s + +s
## ombiliqu�es 8
@adj ombiliqu� +s +e +es
## orbiculaires 7
@adj orbiculaire +s + +s
## pantopor�s 92
@adj pantopor� +s +e +es
## pantropical 55
@adj pantropical pantropicaux +e +es
## paracytiques 84
@adj paracytique +s + +s
## patelliforme 78
@adj patelliforme +s + +s
## pellucides 65
@adj pellucide +s + +s
## penduleux 59
@adj penduleux penduleux penduleuse penduleuses
## penninerv� 85
@adj penninerv� +s +e +es
## pentagonales 5
@adj pentagonal pentagonaux +e +es
## phylog�n�tique 54
@adj phylog�n�tique +s + +s
## planconvexes 70
@adj planconvexe +s + +s
@adj plan-convexe +s + +s
## polliniques 78
@adj pollinique +s + +s
## polyn�sie 80
Polyn�sie	np	[cat=np,@p]
## polysperme 6
@adj polysperme +s + +s
## quadrifide 84
@adj quadrifide +s + +s
## rougegroseille 9
@adj rougegroseille + + +
## scab�rul�es 97
@adj scab�rul� +s +e +es
## spiciformes 7
@adj spiciforme +s + +s
## strobiliforme 50
@adj strobiliforme +s + +s
## subcapit�e 9
@adj subcapit� +s +e +es
## subconique 55
@adj subconique +s + +s
## subcoriace 556
@adj subcoriace +s + +s
## sub�gales 97
@adj sub�gal sub�gaux +e +es
## subenti�res 76
@adj subentier subentiers subenti�re subenti�res
## sub�tal�es 9
@adj sub�tal� +s +e +es
## subferrugineux 5
@adj subferrugineux + subferrugineuse subferrugineuses
## subhomog�ne 60
@adj subhomog�ne +s + +s
## suboppos�es 75
@adj suboppos� +s +e +es
## subsessiles 580
@adj subsessile +s + +s
## subspontan�e 51
@adj subspontan� +s +e +es
## taxonomiques 80
@adj taxonomique +s + +s
## t�tragonaux 66
@adj t�tragonal t�tragonaux +e +es
## trijugu�es 6
@adj trijugu� +s +e +es
## trinervi� 563
@adj trinervi� +s +e +es
## tubercul�es 64
@adj tubercul� +s +e +es
## unifoli�s 75
@adj unifoli� +s +e +es
## uninervi�s 777
@adj uninervi� +s +e +es
## verruculeuses 78
@adj verruculeux + verruculeuse verruculeuses
## x�romorphes 86
@adj x�romorphe +s + +s
## xerophiles 6
@adj xerophile +s + +s
## yaounde 83
##Yaounde	np	[cat=np,@ms]

@adj rotac� +s +e +es
@adj fov�al fov�aux +e +es

@nc pr�c�dente +s @f
@nc pr�c�dent +s @m
@nc esp�ce-type +s @f

@adj uniflore +s + +s
@adj cochl�iforme +s + +s
@adj bipare +s + +s

@adj perenne +s + +s

## for _NPREF-X
@adj flore +s + +s
@adj m�re +s + +s
@adj fide +s + +s
@adj partite +s + +s

Tanganyika	np	[cat=np,@ms]

@adj bilob� +s + +s
@adj monotypique +s + +s
@adj obconique +s + +s
@adj m�ridional m�ridionaux +e +es
@adj spirale +s + +s
@adj �pis�pale +s + +s
@nc �pis�pale +s @f
@adj unisexu� +s +e +es
@adj tubercule +s + +s
@nc raceme +s @m
@adj obov� +s +e +es

@nc recru +s @m

dont	prep	[pred='dont<Obj:sn|sa>']

@adj p�dal� +s +e +es

## ventralement	adv     []

@nc tegula + @f

@adj monocaule +s + +s

@nc brade� +s @f
@nc hypochile +s @m

@adj spongieux + spongieuse spongieuses

@adj lamell� +s +e +es

x	adv	[]

#m�me	adj	[pred='m�me<suj:(sn),obl:(de-sn|de-sinf|de-scompl|�-sn|�-sinf|�-scompl)>',cat=adj,@s,position=ante]
#m�mes	adj	[pred='m�me<suj:(sn),obl:(de-sn|de-sinf|de-scompl|�-sn|�-sinf|�-scompl)>',cat=adj,@p,position=ante]

@nc st�rile +s @f


@redirect mus. mus�e +s
@redirect nat. national nationaux +e +es
@redirect pl. planche__nc +s
@redirect alt. altitude
@redirect soc. soci�t� +s
@redirect hist. histoire
@redirect long. longueur
@redirect m�m. m�moire
@redirect journ. journal journaux
@redirect haut. hauteur
@redirect fr. fruit +s
@redirect gr. graine__nc +s
@redirect fl. fleur +s
@redirect sect. section
@redirect jan. janvier
@redirect f�v. f�vrier
@redirect mar. mars
@redirect avr. avril
@redirect juil. juillet
@redirect sept. septembre
@redirect oct. octobre
@redirect nov. novembre
@redirect dec. decembre
@redirect cat. catalogue__nc +s
@redirect bot. botanique__adj +s + +s
@redirect expl. exploration__nc +s

## Est maintenant dans leffff en tant que adv [kind=prepmod]
## jusque	prepmod	[]

@redirect ....	.

@adj parasite +s + +s

_NUMBER	number	[]

@redirect ter. terminal__adj terminaux +e +es

@redirect A �

@nc frais + @m

@nc amine +s @f

@adj p�doncule +s + +s

@redirect transv. transversal transversaux +e +es

@adj lobule +s + +s

@nc coenosore +s @m

@adj malgache +s + +s

@adj maximum +s + +s
@adj minimum +s + +s
