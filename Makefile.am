## Process this file with automake to produce Makefile.in
AUTOMAKE_OPTIONS = foreign

# configure define the following variables
#       DYACC            DyALog compiler name
#       DYALOG_CFLAGS    Dyalog specific CFLAGS to compile C files with CC
#       DYALOG_DFLAGS    DYACC flags to compile DyALog files
#       DYALOG_LIBS      Link flags for both CC and DYACC
#       DYALOG_CONFIG    DyALog configuration script (dyalog-config)

# additional  variables
DINCLUDES = -I $(srcdir)
DFLAGS    = -autoload -verbose_tag -parse
CFLAGS   += $(DYALOG_CFLAGS)
LIBS     += $(DYALOG_LIBS)
DISTCHECK_CONFIGURE_FLAGS = $(dyalog_configure)

pkgconfigdir = $(libdir)/pkgconfig
dist_pkgconfig_DATA = biomg.pc

EXTRA_DIST = lexed.gram \
	     biogram.smg \
	     preheader.tag \
	     biogram.map \
	     extract.xsl \
	     tagml2html.xsl \
	     style.css \
	     biomg_lexer.in \
	     tree.pl.in \
	     biomg.conf.in \
	     register_parserd.conf.in \
	     MyRegExp.pm \
	     LeffDecoder.yp\
	     tree.js \
	     prototype.js \
	     complete.lex \
	     missing.lex \
	     restrictions.txt \
	     LefffDecoder.yp


.PHONY: html
SUFFIXES = .conf.xml .conf.pl .smg .xml .mg.pl .tag.xml .tag .tex .ps .pdf .pl .o

MAP      = $(srcdir)/biogram.map
MGCOMP   = mgcomp
SMG2XML  = smg2xml
XSLT     = xsltproc
LATEX    = latex
REGISTER = register_parsers
GRAM     = biogram
CHMOD    = chmod

bin_PROGRAMS    = biomg_parser biomg_forest
dist_bin_SCRIPTS = biomg_lexer
pkgdata_DATA    = LefffDecoder.pm \
		  MyRegExp.pm \
		  header.tag \
		  biogram.map \
		  register_parserd.conf \
		  complete.lex \
		  missing.lex \
		  restrictions.txt


sysconf_DATA    = biomg.conf

if WANT_MODPERL
    pkgexecmodperl_DATA = extract.xsl \
		          style.css \
			  tagml2html.xsl \
			  biogram.tag.xml \
			  tree.js \
	                  prototype.js
    pkgexecmodperl_SCRIPTS = tree.pl
endif
pkgexecmodperldir = $(execmodperldir)/$(PACKAGE)


biomg_parser_SOURCES = main.tag tag_generic.pl addons.tag
nodist_biomg_parser_SOURCES = biogram.tag
biomg_parser_DFLAGS = -res header.tag -res tig_header.tag  -res features_header.tag

BUILT_SOURCES = biogram.tag

biomg_forest_SOURCES = easyforest.pl rx_c.c rx.pl
biomg_forest_DFLAGS = $(DYALOG_XML_DFLAGS) ${DYALOG_SQLITE_DFLAGS}
biomg_forest_LDADD = $(DYALOG_XML_LIBS) $(DYALOG_SQLITE_LIBS)



######################################################################

header.tag: biogram.tag.xml $(MAP) $(srcdir)/preheader.tag
	-rm header.tag
	cp $(srcdir)/preheader.tag header.tag
	tag_converter --map=$(MAP) -t header biogram.tag.xml >> $@

tig_header.tag: biogram.tag addons.tag header.tag features_header.tag
	$(DYACC) $(DINCLUDES) -analyze tag2tig $^ -o $@
	cp $@ tmp_header.tag
	$(DYACC) $(DINCLUDES) -analyze lctag $^ tmp_header.tag >> $@

features_header.tag: biogram.tag addons.tag header.tag
	$(DYACC) $(DINCLUDES) -analyze tag_features $^ tig_header.tag.tmp -o $@

biomg_parser-gram.o biomg_parser-main.o biomg_parser-addons.o biomg_parser-tag_generic.o: biogram.tag header.tag tig_header.tag features_header.tag

######################################################################
# Conversions

## MG and grammar files

# Conversions SMG to XMLMG
%.xml: %.smg
	$(SMG2XML) $< > $@

# Converting from XML mg file to LP mg file
%.mg.pl: %.xml
	$(XSLT) -o $@ $(MGTOOLS_XSLT)/mg2dyalog.xsl $<

# Converting from LP mg file to XML TAG file
%.tag.xml: %.mg.pl
	$(MGCOMP) -noforest $< -trytig -xml -family -ht \
				-restore $*.dump -dump $*.dump > $@

# Converting from XML TAG file to DyALog TAG file
%.tag: %.tag.xml
	tag_converter --map=$(MAP) -t lp $< |\
	sed -e '/^%% Family/d' > $@

######################################################################
# Get addition version and information

%.warnings: %.mg.pl
	$(MGCOMP) -noforest $< -warning 2> $@

%.simple: %.mg.pl
	$(MGCOMP) -noforest $< -simple | sort > $@


clean-local:
	-rm biogram.tag.xml biogram.tag header.tag  tig_header.tag features_header.tag
	-rm biogram.mg.pl

LefffDecoder.pm: LefffDecoder.yp
	eyapp -o $@ -m LefffDecoder $<

EDIT = sed \
       -e 's|@bindir\@|$(bindir)|' \
       -e 's|@libdir\@|$(libdir)|' \
       -e 's|@pkgdatadir\@|$(pkgdatadir)|' \
       -e 's|@sysconfdir\@|$(sysconfdir)|' \
       -e 's|@pkgexecmodperldir\@|$(pkgexecmodperldir)|' \
       -e 's|@PACKAGE\@|$(PACKAGE)|' \
       -e 's|@VERSION\@|$(VERSION)|'

tree.pl: tree.pl.in Makefile
	$(EDIT) < $< > $@

register_parserd.conf: register_parserd.conf.in Makefile
	$(EDIT) < $< > $@

biomg.conf: biomg.conf.in Makefile
	$(EDIT) < $< > $@

biomg_lexer: biomg_lexer.in Makefile
	$(EDIT) < $< > $@
	$(CHMOD) 755 $@

## To install biomg.conf without overwriting existing one
install-sysconfDATA: $(sysconf_DATA)
	@$(NORMAL_INSTALL)
	test -z "$(sysconfdir)" || $(mkdir_p) "$(DESTDIR)$(sysconfdir)"
	@list='$(sysconf_DATA)'; for p in $$list; do \
		if test -f "$$p"; then d=; else d="$(srcdir)/"; fi; \
		f=$(am__strip_dir) \
		if [ ! -f $(DESTDIR)$(sysconfdir)/$$f ]; then \
			echo " $(sysconfDATA_INSTALL) '$$d$$p' '$(DESTDIR)$(sysconfdir)/$$f'"; \
			$(sysconfDATA_INSTALL) "$$d$$p" "$(DESTDIR)$(sysconfdir)/$$f"; \
		else \
			echo " *** File exists: '$(DESTDIR)$(sysconfdir)/$$f'"; \
			echo " $(sysconfDATA_INSTALL) '$$d$$p' '$(DESTDIR)$(sysconfdir)/$$f.distrib'"; \
			$(sysconfDATA_INSTALL) "$$d$$p" "$(DESTDIR)$(sysconfdir)/$$f.distrib"; \
		fi; \
	done

# Registering parsers
install-exec-hook:
	$(REGISTER) -a $(srcdir)/register_parserd.conf $(sysconfdir)/parserd.conf

postsvn:
	${MAKE} -t biogram.tag.xml
