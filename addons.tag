/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2004, 2005, 2008 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  addons.tag -- extra TAG trees for biomg
 *
 * ----------------------------------------------------------------
 * Description
 *   we had here trees that can not yet be easily described
 *   just using the metagrammar
 *
 *   Note that the trees described here can not be visualized
 *
 * ----------------------------------------------------------------
 */

:-include('header.tag').

tag_tree{ name => start,
	  family => ht{ cat => (-) },
	  tree => tree -start('$pos'(N),
			      { N= 0
			      xor 'C'(_,lemma{ lex => ponct[':','�',';','.','�','(',')','!','?','!?','?!','!!!','_SENT_BOUND']}, N)
			      }
			     )
	}
.

tag_tree{ name => end,
	  family => ht{ cat => (-) },
	  tree => tree -end('$pos'(N), {recorded('N'(N))})
	}
.


tag_tree{ name => incise,
	  family => ht{ cat => (-) },
	  tree => auxtree top = X at -incise(
					     (	 [','] ;
						 '$pos'(S),
						 { S = 0 
						 xor 'C'(_,lemma{ cat => coo },S)
						 xor 'C'(_,lemma{ lex => '_' },S)
						 }
					     ),
					     top = X::incise{ incise_kind => coma } at *incise,
					     (	 [','] ;
						 (   '$pos'(N),
						     { recorded('N'(N))
						     xor   'C'(N,lemma{ cat => ponctw },_)
						     xor   'C'(N,lemma{ cat => poncts },_)
						     }
						 )
					     )
					    )
	}
.


tag_tree{ name => par_incise,
	  family => ht{ cat => (-) },
	  tree => auxtree top=X at -incise(
					   [ponct['(','[']],
					   top = X::incise{ %% position => post,
							    incise_kind => par }
					   at *incise,
					   [ponct[')',']','_SMILEY']]
					  )
	}
.

tag_tree{ name => dash_incise,
	  family => ht{ cat => (-) },
	  tree => auxtree top=X at -incise(
					   [initponct[]],
					   top = X::incise{ %% position => post,
							    incise_kind => dash
							  } at *incise,
					   (   [initponct[]] ;
					       '$pos'(N),
					       { recorded('N'(N))
					       xor 'C'(N,lemma{ cat => poncts },_)
					       }
					   )
					  )
	}
.


tag_tree{ name => unknown,
	  family => ht{},
	  tree => tree -unknown( [X] )
	}
.

tag_tree{ name => unknown_wponct,
	  family => ht{},
	  tree => tree -unknown( - <=> ponctw )
	}
.

tag_tree{ name => unknown_sponct,
	  family => ht{},
	  tree => tree -unknown( - <=> poncts )
	}
.

tag_tree{ name => unknown_adv,
	  family => ht{},
	  tree => tree -unknown( <=> adv )
	}
.

tag_tree{ name => unknown_csu,
	  family => ht{},
	  tree => tree -unknown( - <=> csu )
	}
.

tag_tree{ name => unknown_pri,
	  family => ht{},
	  tree => tree -unknown( - <=> pri )
	}
.

tag_tree{ name => unknown_prel,
	  family => ht{},
	  tree => tree -unknown( - <=> prel )
	}
.

tag_tree{ name => unknown_pro,
	  family => ht{},
	  tree => tree -unknown( - <=> pro )
	}
.

tag_tree{ name => unknown_adj,
	  family => ht{},
	  tree => tree -unknown( <=> adj )
	}
.

tag_tree{ name => unknown_advneg,
	  family => ht{},
	  tree => tree -unknown( - <=> advneg )
	}
.

tag_tree{ name => unknown_prep,
	  family => ht{},
	  tree => tree -unknown( - <=> prep )
	}
.

tag_tree{ name => unknown_coo,
	  family => ht{},
	  tree => tree -unknown( - <=> coo )
	}
.

tag_tree{ name => unknown_v,
	  family => ht{},
	  tree => tree -unknown( - <=> v )
	}
.

tag_tree{ name => follow_coord,
	  family => ht{},
	  tree => tree strace( '$pos'(N), { 'C'(_,lemma{ cat => coo },N ) } )
	}
.

