/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2003, 2004, 2005, 2006, 2007, 2008 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_gneric.pl -- Generic Stuff for TAGs
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-scanner(check_lexical).
:-skipper(skip_lexical).
	
:-parse_mode(token).

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-features( lemma, [lex,lemma,cat,top,anchor,truelex] ).

:-rec_prolog normalized_tag_lemma/5.
:-std_prolog anchor/6.
:-extensional tag_lexicon/4.

:-extensional tag_hypertag/2.

:-light_tabular
	verbose!lexical/5,
	verbose!struct/2,
	verbose!anchor/7,
	verbose!coanchor/5,
	verbose!adj/0,
	verbose!epsilon/4
	.

:-extensional autoload_check_hypertag/3.

:-require('format.pl').

:-op(  700, xfx, [?=]). % for default value
:-xcompiler(( X ?= V :- ( \+ var(X) xor X = V))). %% Setting a default value

anchor(tag_anchor{ name => HyperTag,
		   coanchors => VarCoanchors,
		   equations => VarEquations
		 },
       Token,
       Cat,
       Left,
       Right,
       Top
      ) :-
%%	Top = _Top,
%%	format( 'LOOKING ANCHOR left=~w ht=~w top=~w\n',[Left,HyperTag,Top] ),
	'C'( Left,
	     Info::lemma{ lex=> Token,
			  cat => Cat,
			  top => _Top,
			  lemma => Lemma,
			  anchor => tag_anchor{ name => HyperTag,
						coanchors => Coanchors,
						equations => VarEquations
					      }
			},
	     Right
	   ),
	(   cat_normalize(Cat,_Top),
	    %%	    format('Normalize ~w => ~w\n',[Cat,_Top]),
	    true
	xor true),
	Top = _Top,
	%%	(   var(Lemma) xor check_all_coanchors(VarCoanchors,Coanchors)),
%%	format( 'ANCHOR ~w ~w ~w\n',[Left,Right,Info] ),
	true
	.

:-std_prolog coanchor/5.

coanchor(Token,Cat,Left,Right,Top) :-
%%	Top = _Top,
	'C'(Left,
	   Info::lemma{ lex => Token,
			cat => Cat,
			top => _Top },
	    Right
	   ),
	(   cat_normalize(Cat,_Top) xor true),
	Top = _Top,
%%	format( 'COANCHOR ~w ~w ~w\n',[Left,Right,Info] ),
	true
	.

:-rec_prolog cat_normalize/2.

:-rec_prolog check_all_coanchors/2.

check_all_coanchors([],[]).

check_all_coanchors([VarLemma|VL],[Lemma|L]) :-
	( Lemma == [] ->
	    (	VarLemma = [] xor true)
	;   var(VarLemma) ->
	    VarLemma = Lemma
	;   VarLemma = check_at_anchor(Left,Right) ->
	    check_coanchor_lemma(Lemma,Left,Right)
	;   
	    fail
	),
	check_all_coanchors(VL,L).

:-std_prolog check_coanchor/3.

check_coanchor(VarLemma,Left,Right) :-
	( var(VarLemma) ->
	    VarLemma = check_at_anchor(Left,Right)
	;   VarLemma == [] ->
	    true
	;   
	    check_coanchor_lemma(VarLemma,Left,Right)
	)
	.

:-std_prolog check_coanchor_in_anchor/2.

check_coanchor_in_anchor(VarLemma,VarLemma).

:-std_prolog check_coanchor_lemma/3.

check_coanchor_lemma(Lemma,Left,Right) :-
	domain(Token,Lemma),
	'C'(Left,lemma{ lex=>Token },Right)
	.

:-std_prolog skip_lexical/3.
:-finite_set(skipcat,[epsilon,sbound]).
skip_lexical(Left,Token,Right) :-
	'C'(Left,lemma{ lex => Token, cat => skipcat[] }, Right ),
%%	format('Found epsilon ~w ~w\n',[Left,Token]),
	true
	.

verbose!epsilon(_Lex,Left,Right,[_Lemma,_Lexical]) :-
	'C'(Left,lemma{lex => _Lex, truelex => _Lexical, cat => skipcat[], lemma => _Lemma }, Right),
	_Lemma ?= '_Uv'
	.

:-std_prolog check_lexical/3.

check_lexical(Left,Token,Right) :-
%%	format('Try check lexical ~w ~w\n',[Left,Token]),
	'C'(Left,
	    lemma{ lex => Lex,
		   truelex => TrueLex,
		   lemma => Lemma
		 },
	    Right),
	( var(Token) ->
	  Lex = Token
	;
	  (   Lex = Token
	  xor TrueLex = Token
	  xor \+ var(Lemma),
	      Token = Lemma
	  ),
	  %%	format('Checked lexical ~w ~w => ~w\n',[Left,Token,Right]),
	  %%	'C'(Left,lemma{ cat => Token },Right)
	  true
	)
	.

%% Dummy predicate used to leave a trace in the forest
verbose!lexical([_Lexical],_Left,_Right,_Cat, [_Lemma,_TrueLex]) :-
	'C'(_Left,lemma{ lex => _Lex, truelex => _TrueLex , lemma => _Lemma}, _Right),
	( _Lex == _Lexical
	xor _TrueLex == _Lexical
	xor _Lemma == _Lexical ),
	_Lemma ?= '_Uv'
	.

/*
verbose!lexical([L::(_;_)],_Left,_Right,_Cat, [_Lemma,_TrueLex]) :-
%%	format('Check verbose lexical ~w\n',[L]),
	check_verbose_lexical(L,_Left,_Right,_Cat,[_Lemma,_TrueLex]).
*/

:-std_prolog check_verbose_lexical/5.

check_verbose_lexical(L,_Left,_Right,_Cat,[_Lemma,_TrueLex]) :-
	( L = (A1;A2) ->
	  ( check_verbose_lexical(A1,_Left,_Right,_Cat,[_Lemma,_TrueLex])
	  ;  check_verbose_lexical(A2,_Left,_Right,_Cat,[_Lemma,_TrueLex])
	  )
	; L = [_Lexical],
	  'C'(_Left,lemma{ lex => _Lex, truelex => _TrueLex , lemma => _Lemma}, _Right),
	  ( _Lex == _Lexical
	  xor _TrueLex == _Lexical
	  xor _Lemma == _Lexical ),
	  _Lemma ?= '_Uv'
	)
	.

%% Dummy predicate used to leave a trace in the forest
verbose!struct(_Tree_Name,_HyperTag) :-
%%	format('Recognized ~w\n',[_Tree_Name]),
	true
	.

%% Dummy predicate used to leave a trace in the forest
verbose!anchor(_Anchor,_Left,_Right,_Tree_Name,_Top, [_Lemma,_Lex], tag_anchor{ name => HT }) :-
%%	format('VERBOSE ANCHOR anchor=~w left=~w right=~w lemma=~w lex=~w\n\tht=~w\n\ttop=~w\n',
%%	       [_Anchor,_Left,_Right,_Lemma,_Lex,HT,_Top]),
	'C'(_Left,lemma{ top => __Top,
			 cat => _Cat,
		         truelex => _Lex,
			 lex => _Anchor,
			 lemma => _Lemma,
			 anchor => tag_anchor{ name => HT }
		       }, _Right),
	(   cat_normalize(_Cat,__Top) xor true ),
	_Top = __Top,
	_Lemma ?= '_Uv'
	.

%% Dummy predicate used to leave a trace in the forest
verbose!coanchor(_Lex,_Left,_Right,_Top,[_Lemma,_TrueLex]) :-
	'C'(_Left,lemma{ top => __Top, cat => _Cat, lex => _Lex, truelex => _TrueLex, lemma => _Lemma}, _Right),
	(   cat_normalize(_Cat,__Top) xor true ),
	_Top = __Top,
%%	format('VERBOSE COANCHOR ~w ~w ~w ~w\n',[_Left,_Lemma,_TrueLex,_Right]),
	_Lemma ?= '_Uv'
	.

%% Dummy predicate used to leave a trace in the forest
verbose!adj.

:-rec_prolog normalize_coanchors/1.

normalize_coanchors([]).
normalize_coanchors([VarCoanchors|VC]) :-
	(VarCoanchors = [] xor true ),
	normalize_coanchors(VC)
	.

:-std_prolog tag_family_load/5.

tag_family_load(HyperTag,Cat,Top,Token,Name) :-
%%	format('TAG FAMILY LOADER ~w ~w\n',[Cat,HyperTag]),
	(   %%format('** Hypertag = ~w\n',[Hypertag]),
	    'C'(_,
		lemma{ cat => Cat,
		       top => _Top,
		       lex => Lex,
		       lemma => Lemma,
		       anchor => tag_anchor{ name => _HyperTag }
		     },
		_),
	    (	cat_normalize(Cat,_Top) xor true),
	    %% Next line to handle support verbs (should find a more elegant way)
	    %% The trouble is that support verbs inherits their frame from arg1
	    %% but this inheritance is done after the autoloading phase (via clean_sentences)
	    (	HyperTag = _HyperTag xor autoload_check_hypertag(Cat,HyperTag,_HyperTag)),
	    \+ \+ ( Top = _Top,
		      (	  Lex = Token
		      xor (\+ var(Lemma), Lemma = Token)
		      )),
	    assert_anchor_points(Cat,HyperTag,Top,Name),
%%	    format('--> To be loaded ~w ~w\n',[Cat,HyperTag]),
	    true
	)
	xor fail.

:-std_prolog assert_anchor_points/4.

assert_anchor_points(Cat,HyperTag,Top,Name) :-
	every(( 'C'(Left,
		    lemma{ cat => Cat,
			   top => Top,
			   anchor => tag_anchor{ name => Hypertag }
			 },
		    _
		   ),
		( recorded( anchor_point(Name,Left) )
		xor record( anchor_point(Name,Left) )
		)
	      )),
	true
	.
	
:-std_prolog tag_check_coanchor/3.

tag_check_coanchor(Cat,Top,Token) :-
	(   'C'(_,
		lemma{ cat => Cat, lex => Lex, lemma => Lemma, top => _Top },
		_),
	    (	cat_normalize(Cat,_Top) xor true),
	    \+ \+ ( Top = _Top,
		      (	  Lex = Token
		      xor (\+ var(Lemma), Lemma = Token)
		      )),
	    true
	)
	xor fail.

:-extensional ok_tree/2.
:-extensional lctag/4.
%%:-light_tabular tag_filter/1.

:-extensional lctag_ok/1.
:-extensional lctag_map/3.
:-extensional ok_cat/3.
:-extensional loaded_tree/1.

:-std_prolog check_ok_cat/3.

check_ok_cat(Cat,Mode,Left) :-
	%% format('Check ok cat ~w ~w ~w\n',[Cat,Mode,Left]),
	( var(Left) ->
	  ( lctag_map(Cat,Mode,Tree),
	    ( lctag_ok(Tree)  xor check_ok_tree(Tree,Left))
	  xor fail
	  )
	;
	  ok_cat(Cat,Mode,Left) ->
	  true
	; lctag_map(Cat,Mode,Tree),
	  ( lctag_ok(Tree)  xor check_ok_tree(Tree,Left))
	  ->
	  record(ok_cat(Cat,Mode,Left)),
	  true
	;
	  fail
	).


:-std_prolog check_ok_tree/2.

check_ok_tree(Name,Left) :-
	( var(Left) ->
	  ( ok_tree(Name,_Left) xor fail )
	;  ok_tree(Name,Left) ->
	  true
	),
%%	format('selected ~w ~w\n',[Left,Name] ),
	true
	.

tag_filter(Left) :-
%%	format('Try LC selected ~w ~w\n',[Left,Name]),
	'C'(Left,
	    lemma{ lex => Lex,
		   truelex => TrueLex,
		   lemma => Lemma,
		   cat => Cat
		 },
	    Right),
	( ( lctag(cat,Cat,Name,Trees),
	    Kind = cat,
	    V = Cat
	  ; lctag(scan,Lex,Name,Trees),
	    Kind = scan,
	    V = Lex
	  ; Lex \== TrueLex,
	    lctag(scan,TrueLex,Name,Trees),
	    Kind = scan,
	    V = TrueLex
	  ; lctag(scan,Lemma,Name,Trees),
	    Kind = scan,
	    V = Lemma
	  ; Cat = skipcat[],
	    '$answers'(tag_filter(Right)),
	    ok_tree(Name,Right),
	    Kind = skip
	  ),
	  \+ recorded(OK::ok_tree(Name,Left)),
	  (Kind == skip xor check_anchor_point(Name,Left)),
	  record(OK),
	  fail
	%%	  format('ok_tree left=~w tree=~w\n',[Left,Name]),
	;
	  %%	tabulated_tag_filter(Name,Left),
	  %%	format('LC selected ~w ~w\n',[Left,Name]),
	  true
	)
	.


:-std_prolog check_anchor_point/2.
%%:-light_tabular check_anchor_point/5.

check_anchor_point(Name,Left) :-
%%	recorded(loaded_tree(Name)),
	( \+ recorded(anchor_point(Name,_)) ->
	  true
	; recorded(anchor_point(Name,_Left)),
	  Left =< _Left   ->
	  true
	;
	  fail
	)
	.

/*
	( Trees = [] ->
	  true
 	; domain(T1,Trees),
 	  recorded( ok_tree(T1,Left) ) ->
 	  true
	; domain(T1,Trees),
%%	  lctag(Kind,V,T1,Trees1),
	  Trees1=[],
	  check_anchor_point(T1,Left,Trees1,Kind,V) ->
	  record( ok_tree(T1,Left) )
	;
	  fail
	)
	.
*/
