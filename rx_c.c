/** 
 * ----------------------------------------------------------------
 * $Id$
 * Copyright (C) 2004, 2005, 2006 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  \file  rx.c 
 *  \brief Regular Expression API for DyALog
 *
 * ----------------------------------------------------------------
 * Description
 * Inspired from rx package from Van Noord
 * http://odur.let.rug.nl/~vannoord/prolog-rx/
 * ----------------------------------------------------------------
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include "libdyalog.h"
#include "builtins.h"


#ifndef HAVE_STRNDUP

size_t
my_strnlen (const char *string, size_t maxlen)
{
  const char *end = memchr (string, '\0', maxlen);
  return end ? (size_t) (end - string) : maxlen;
}

char *
strndup (s, n)
     const char *s;
     size_t n;
{
  size_t len = my_strnlen (s, n);
  char *new = malloc (len + 1);

  if (new == NULL)
    return NULL;

  new[len] = '\0';
  return (char *) memcpy (new, s, len);
}

#endif

regex_t *regcomp_pl(const char *s1,
                    const int reg_extended,
                    const int reg_newline,
                    const int reg_icase,
                    const int reg_nosub)
{
  regex_t r;
  int err;
  int flags=0;
  regex_t *t = (regex_t *)malloc(sizeof(r));
  if (reg_extended) flags=flags|REG_EXTENDED;
  if (reg_newline)  flags=flags|REG_NEWLINE;
  if (reg_icase)    flags=flags|REG_ICASE;
  if (reg_nosub)    flags=flags|REG_NOSUB;

  err = regcomp (t, s1, flags);
  if (err)
    {
      char errmsg[100];
      regerror (err, t, errmsg, 100);
      puts (errmsg);
      regfree (t);
      free (t);
      dyalog_error_printf( "error: %s", errmsg );
      return 0;
    }
  else
    return t;
}

void regfree_pl(regex_t *r) {
  regfree(r);
  free(r);
}

Bool
regexec_pl(char *s2,             /* string to be matched    */
           const int reg_notbol,
           const int reg_noteol,
           regex_t *r,           /* compiled expression     */
           const int need_subs,  /* return matched subs ?
                                  * should be 0 if r compiled
                                  * with REG_NOSUB
                                  */
           sfol_t res
           ){
    int err;
    int flags=0;
    int subs=(*r).re_nsub;
    regmatch_t regs[subs+1];
    char errmsg[100];

    if (reg_notbol) flags=flags|REG_NOTBOL;
    if (reg_noteol) flags=flags|REG_NOTEOL;
    err = regexec (r, s2, subs+1, regs, flags);
    switch (err) {
        case 0:
        {
            fol_t t = FOLNIL;
            if (need_subs) {
                int i;
                for (i=subs;i>0;i--){
                    t = Create_Pair(Create_Atom((char *)strndup(&s2[regs[i].rm_so],
                                                                regs[i].rm_eo-regs[i].rm_so)
                                                ),
                                    t);
//                    dyalog_printf("Get Subs Exp %d %d => %&f\n",subs,i,t);
                }
            }
            return sfol_unify( t, Key0, res->t, res->k );
        }
        
        case REG_NOMATCH:
        {
            Fail;
        }
        
        default:
        {
            regerror (err, r, errmsg, 100);
            dyalog_error_printf("Error in regular expression %s",errmsg);
            Fail;
        }
    }
}

static fol_t
strtok_aux(char * s,
           const char *delims) 
{
    char *token = strtok(s,delims);
    if (!token) {
        return FOLNIL;
    } else {
        return Create_Pair(Create_Atom((char *)strdup(token)),strtok_aux(NULL,delims));
    }
}

    

Bool
DyALog_strtok( const char *s,
               const char *delims,
               sfol_t res ) 
{
    char s2[strlen(s)+1];
    strcpy(s2,s);
    return sfol_unify( strtok_aux( s2, delims), Key0, res->t, res->k);
}
